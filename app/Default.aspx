<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:mso="urn:schemas-microsoft-com:office:office" xmlns:msdt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882">

<head>
<!--[if gte mso 9]><xml>
<mso:CustomDocumentProperties>
<mso:ContentTypeId msdt:dt="string">0x010100B5056DEB8FB70A45B6D8D429BCD4192C</mso:ContentTypeId><mso:ContentType msdt:dt="string">Document</mso:ContentType><mso:Approval_x0020_Level msdt:dt="string"></mso:Approval_x0020_Level><mso:Categories msdt:dt="string"></mso:Categories><mso:Assigned_x0020_To msdt:dt="string"></mso:Assigned_x0020_To></mso:CustomDocumentProperties></xml><![endif]-->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="mobile-web-app-capable" content="yes" />
		<meta name="apple-mobile-web-app-title" content="Vote 2017" />
		<link rel="canonical" href="https://www.calgary.ca/election/Scripts/pwa/">

		<base href="/election/Scripts/pwa/" />
		<!--base href="/" /-->
		
		<title>Vote 2017</title>
		<meta name="DC.creator" content="Election App" />
		<meta name="WT.ti" content="Vote 2017" />
		
		<!-- generated with RealFaviconGenerator: http://realfavicongenerator.net -->
		<link rel="apple-touch-icon" sizes="180x180" href="images/icons/apple-touch-icon.png?v1.145" />
		<link rel="icon" type="image/png" sizes="32x32" href="images/icons/favicon-32x32.png?v1.145" />
		<link rel="icon" type="image/png" sizes="16x16" href="images/icons/favicon-16x16.png?v1.145" />
		
		<link rel="mask-icon" href="images/icons/safari-pinned-tab.svg" color="#5bbad5" />
		<meta name="theme-color" content="#ffffff" />
		<!-- added additional files because they are present in this folder location, not website root: -->
		<meta name="msapplication-config" content="browserconfig.xml" />
		<link rel="icon" href="favicon.ico" />
		<!--
		<link rel="shortcut icon" sizes="16x16" href="images/elections/icon-16x16.png?v2" />
		<link rel="shortcut icon" sizes="196x196" href="images/elections/icon-196x196.png?v2" />
		<link rel="apple-touch-icon-precomposed" href="images/elections/icon-152x152.png?v2" />
		-->

		<link rel="manifest" href="manifest.webmanifest" />
		<script src="libs/pwacompat/pwacompat.min.js" type="text/javascript"></script><!-- polyfill for ios so it can use some manifest settings - used so that start_url setting would work -->
		
    	<!-- build:css styles/main.css -->
		<link href="/Scripts/PatternLibrary/v1.2-calgaryca-custom/COC.Pattern.Library.css?v1.145" type="text/css" rel="stylesheet" /><!-- reference PL in calgary.ca root -->
		<link href="libs/jquery-ui-1.12.1.custom.accordion-only/jquery-ui.min.css?v1.145" type="text/css" rel="stylesheet" /><!--jquery-ui autocomplete used by find my ward -->
		<link href="libs/jquery-ui-1.12.1.custom.accordion-only/jquery-ui.structure.min.css?v1.145" type="text/css" rel="stylesheet" /><!--jquery-ui autocomplete used by find my ward -->
		<link rel="stylesheet" type="text/css" href="libs/addtohomescreen/addtohomescreen.custom.css?v1.145" /><!-- customized add-to-home plugin -->
		<!--coc-styles-ele.css included the new pattern - action panel. Replace it after the mobile navigation tested  -->  
		<link href="/Scripts/PatternLibrary/v1.2-calgaryca-custom/coc-styles-ele.css?v1.145" rel="stylesheet" type="text/css" />
		<!--link href="coc-styles-ele.css?v1.145" rel="stylesheet" type="text/css" /-->
		<link href="/Scripts/MasterPage/COC.Global.Election.Overrides.css?v1.145" rel="stylesheet" type="text/css" />
		<link href="elections.css?v1.1454" rel="stylesheet" type="text/css" />
		<!-- endbuild -->
		
		<!--script src="https://www1test.calgary.ca/sdc/WTWirelessApps.load.js" type="text/javascript"></script-->
		<script src="https://www1.calgary.ca/sdc/WTWirelessApps.load.js" type="text/javascript"></script>
	</head>
	<body class="coc-wrapper coc-pl">
	<!--body class="coc-wrapper coc-pl" style="opacity:0;"-->
  
    <!-- build:js scripts/main.min.js -->
	<script src="scripts/main.min.js"></script>
	<!-- endbuild -->
  	
  	<!-- Preload images: -->
  	<img src="images/icons/apple-touch-icon.png?v1.145" style="display:none;" />
  	<img src="images/elections/people-icon.svg" style="display:none;" />
  	<img src="images/elections/form-icon.svg" style="display:none;" />
	<img src="images/elections/card-icon.svg" style="display:none;" />
	<img src="images/elections/map-icon.svg" style="display:none;" />
	
    <header class="cui coc-header coc-header-withNavOnly ele-header mobile-view-only">
    	<a id="main-icon-link" href="Default.aspx#" class="coc-logo">
		    <img src="images/elections/2017-VoteElections.svg" />
		</a>
    	<h1>Vote 2017 (wt test)</h1><!-- webtrends test url is being used -->
    	<!--NOTE: Accessible links not yet made to work with app shell -->
    	<ul id="skiplinks">
        	<li><a href="#content" accesskey="S">Skip to main content</a></li>
      	</ul>          
		<nav class="cui coc-secondary-9L-nav" role="navigation" id="navId">
			<ul class="nav-menu">
				<button type="button" id="exploreBtn" class="navBtnMobile" aria-controls="navId" aria-haspopup="true" aria-label="menu expanded">Explore<span class="cicon-bars"></span></button>
				<button type="button" id="closeBtn" class="navBtnMobile" aria-controls="navId" aria-haspopup="true" aria-label="menu collapsed">Close<span class="cicon-times"></span></button>
				<li class="nav-item homeScreenBtn pwa-hide">
					<span>Add the web app to your mobile device home screen for easy access:</span>
					<a id="btnAddToHome" href="" data-event="NavGlobal" data-comment="Add App To Home Screen Button" class="coc-secondary-9L menu-btn view-button" tabindex="0"> Add App to Home Screen</a>
				</li>
				<li class="nav-item">
					<a href="/election/Pages/home.aspx" target="_blank" data-event="NavGlobal" class="coc-secondary-9L external-link" tabindex="0"> Visit the full website for more information on the 2017 Election <span class="cicon-external-link"></span></a>
				</li>
				<li class="nav-item">
					<a href="/General/Pages/Terms-of-Use.aspx" target="_blank" data-event="NavGlobal" class="coc-secondary-9L external-link" tabindex="0"> Terms &amp; conditions <span class="cicon-external-link"></span></a>
				</li>
				<li class="nav-item">
					<a href="/General/Pages/Privacy-Policy.aspx" target="_blank" data-event="NavGlobal" class="coc-secondary-9L external-link" tabindex="0"> Privacy Policy <span class="cicon-external-link"></span></a>
				</li>
			</ul>
		</nav>
    </header>
    <!-- /Header -->
    
		<!-- Action Panel -->
		<div class="mobileBottomNav active">
			<nav class="action-panel cui active expanded" role="navigation">
				<span class="action-bar active">
					<a type="button" class="action-button expandBtn active" aria-controls="" aria-haspopup="true" aria-label="menu expanded" tabindex="0"style=" display: none;"><span>Voting</span></a>
					<a type="button" class="action-button collapseBtn" aria-controls="" aria-haspopup="true" aria-label="menu collapsed" tabindex="0"><span>Voting</span></a>
				</span>

				<ul class="action-items-container">
					<span class="action-items"> 
						<li>
							<button id="btnUpdate" data-view-sel="#viewSectionUpdate_Latest" data-view-title="Updates" data-event="NavBottom" data-comment="Updates" data-txt-src="content/updates/articles.txt" class="action view-button" aria-label="Get updates related to election">
								<span id="news-icon" class="icon-placeholder top" aria-hidden="true"></span>
								News
							</button>
						</li>
						<li>
							<button id="btnCandidates" data-view-sel="#viewCandidates" data-view-title="Candidates" data-event="NavBottom" data-comment="Candidates" data-txt-src="content/candidates/landing-page.txt" class="action view-button" aria-label="Learn about candidates">
								<span id="candidate-icon" class="top icon-placeholder" aria-hidden="true"></span>
								Candidates
							</button>
						</li>
						<li>
							<!--Old version: button id="btnWhyVote" data-view-sel="#viewVoterInfo" data-view-title="Why Vote" data-event="NavBottom" data-comment="Why Vote" class="action view-button" aria-label="Learn why your vote counts"-->
							<button id="btnWhyVote" data-view-sel="#viewSectionVoterInfo_WhyVote" data-view-title="Why Vote" data-event="NavBottom" data-comment="WhyVote" data-txt-src="content/voterInfo/why-vote.html.txt" class="action view-button" aria-label="Learn why your vote counts">
								<span id="vote-information-icon" class="icon-placeholder top" aria-hidden="true"></span>
								Voting information
							</button>
						</li>
						<li>
							<!--Previously, this pointed to "When/Where To Vote" section of Info for Voters: 
							<button id="btnVote" data-view-sel="#viewSectionVoterInfo_WhenWhereToVote" data-view-title="When/Where do I vote" data-event="NavBottom" data-comment="When/Where do I vote" data-txt-src="content/voterInfo/when-and-where-do-i-vote.html.txt" class="action view-button active" aria-label="Learn how and where to vote">-->
							<button id="btnVote" data-view-sel="#viewVote" data-view-title="Vote" data-event="NavBottom" data-comment="Vote" class="action view-button active" aria-label="Learn how and where to vote">
								<span id="where-vote-map-icon" class="icon-placeholder top" aria-hidden="true"></span>
								Where Can I Vote?
							</button>
						</li>
					</span>
				</ul>

			</nav>
    </div>


    <!-- Main app content starts here -->
    <main id="content" class="container-fluid">
	  <div id="cocis-nav-active-page-overlay" style="display:none;"></div>
	  
      <section id="viewVoterInfo" class="view">
      	<!--local navigation menu-->
				<div id="localNavContainer" class="localNavContainer">
					<div class="col-md-12 no-padding">
						<nav class="cui topicNav localNav mobile-view-only" role="navigation" id="localNav">
							<ul class="nav-menu">
								<button type="button" id="exploreBtn_local" class="navBtnMobile_topic expandBtn" aria-controls="localNav" aria-haspopup="true" aria-label="menu expanded">Information for voters menu</button>
								<button type="button" id="closeBtn_local" class="navBtnMobile_topic collapseBtn" aria-controls="localNav" aria-haspopup="true" aria-label="menu collapsed" style="display:none;">Information for voters menu</button>
								<li class="nav-item active">
											<!--<a href="/election/Pages/information-for-voters/default.aspx">Information for Voters</a>-->
											<ul role="group" aria-expanded="true" aria-hidden="false" class="sub-nav">
												<li class=""> <a class="view-button" data-view-sel="#viewSectionVoterInfo_WhoCanVote" data-view-title="Who can vote" data-event="NavVoterInfo" data-comment="Who can vote" data-iframe-src="election/Pages/information-for-voters/who-can-vote.aspx?IsDlg=1&iframe=1" data-txt-src="content/voterInfo/who-can-vote.html.txt">Who can vote?</a></li>
												<li class=""> <a class="view-button" data-view-sel="#viewSectionVoterInfo_WhyVote" data-view-title="Why vote" data-event="NavVoterInfo" data-comment="Why vote" data-iframe-src="election/Pages/information-for-voters/why-vote.aspx?IsDlg=1&iframe=1" data-txt-src="content/voterInfo/why-vote.html.txt">Why vote?</a></li>
												<li class=""> <a class="view-button" data-view-sel="#viewSectionVoterInfo_HowToVote" data-view-title="How to vote" data-event="NavVoterInfo" data-comment="How to vote" data-iframe-src="election/Pages/information-for-voters/how-to-vote.aspx?IsDlg=1&iframe=1" data-txt-src="content/voterInfo/how-to-vote.html.txt">How to vote</a></li>
												<li class=""> <a class="view-button" data-view-sel="#viewSectionVoterInfo_WhenWhereToVote" data-view-title="When/Where do I vote" data-event="NavVoterInfo" data-comment="When/Where do I vote" data-iframe-src="election/Pages/information-for-voters/when-and-where-do-i-vote.aspx?IsDlg=1&iframe=1" data-txt-src="content/voterInfo/when-and-where-do-i-vote.html.txt">When/Where do I vote?</a></li>
												<li class=""> <a class="view-button" data-view-sel="#viewSectionVoterInfo_AccessibleVoting" data-view-title="Accessible Voting" data-event="NavVoterInfo" data-comment="Accessible Voting" data-iframe-src="election/Pages/information-for-voters/accessible-voting.aspx?IsDlg=1&iframe=1" data-txt-src="content/voterInfo/accessible-voting.html.txt">&nbsp;&nbsp;&nbsp;&nbsp;Accessible Voting</a></li>
												<li class=""> <a class="view-button" data-view-sel="#viewSectionVoterInfo_IdentificationRequirements" data-view-title="Identification Requirements" data-event="NavVoterInfo" data-comment="Identification Requirements" data-iframe-src="election/Pages/information-for-voters/identification-requirements.aspx?IsDlg=1&iframe=1" data-txt-src="content/voterInfo/identification-requirements.html.txt">Identification Requirements</a></li>
												<li class=""> <a class="view-button" data-view-sel="#viewSectionVoterInfo_WardBoundaryChanges" data-view-title="Ward Boundary Changes" data-event="NavVoterInfo" data-comment="Ward Boundary Changes" data-iframe-src="election/Pages/information-for-voters/ward-boundary-changes.aspx?IsDlg=1&iframe=1" data-txt-src="content/voterInfo/ward-boundary-changes.html.txt">Ward Boundary Changes</a></li>
												<li class=""> <a class="view-button" data-view-sel="#viewSectionVoterInfo_FindMyWard" data-view-title="Find my Ward" data-event="NavVoterInfo" data-comment="Find my Ward" data-iframe-src="election/Pages/information-for-voters/find-my-ward.aspx?IsDlg=1&iframe=1" data-txt-src="content/voterInfo/find-my-ward.html.txt">&nbsp;&nbsp;&nbsp;&nbsp;Find my Ward</a></li>
												<li class=""> <a class="view-button" data-view-sel="#viewSectionVoterInfo_NewWardBoundaries" data-view-title="New Ward Boundaries" data-event="NavVoterInfo" data-comment="New Ward Boundaries" data-iframe-src="election/Pages/information-for-voters/new-ward-boundaries-oct-16.aspx?IsDlg=1&iframe=1" data-txt-src="content/voterInfo/new-ward-boundaries-oct-16.html.txt">&nbsp;&nbsp;&nbsp;&nbsp;New Ward Boundaries</a></li>
												<!--li class=""> <a class="external-link" data-event="NavVoterInfo" href="http://www.calgary.ca/CA/city-clerks/Pages/Election-and-information-services/Outreach/voter-info.aspx" target="_blank">Outreach Program <span class="cicon-external-link"></span></a></li-->
											</ul>
										</li>
									</ul>
						</nav>
					</div>
				</div>
      	<!--local nav menu end-->
        <section id="viewSectionVoterInfo_WhoCanVote" class="view">
				</section>
        <section id="viewSectionVoterInfo_WhyVote" class="view main-child">
        </section>
        <section id="viewSectionVoterInfo_HowToVote" class="view">
        </section>
        <section id="viewSectionVoterInfo_WhenWhereToVote" class="view">
        </section>
        <section id="viewSectionVoterInfo_IdentificationRequirements" class="view">
        </section>
        <section id="viewSectionVoterInfo_WardBoundaryChanges" class="view">
        </section>
        <section id="viewSectionVoterInfo_FindMyWard" class="view">
        </section>
        <section id="viewSectionVoterInfo_NewWardBoundaries" class="view">
        </section>
        <section id="viewSectionVoterInfo_AccessibleVoting" class="view">
        </section>
        <section id="viewSectionVoterInfo_OutreachProgram" class="view">
        </section>
      </section>

      <section id="viewVote" class="view">
        <!--header>
          <button data-view-sel="#viewSectionVote_How" class="cui btn-sm primary-ghost">How</button>
          <button data-view-sel="#viewSectionVote_Where" class="cui btn-sm">Where</button>
        </header-->
        <section id="viewSectionVote_How" class="view main-child">
        	<div class="subtitle-block cui election-subtitle-block">
						<h2>Voting</h2>
						<hr />
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="cui tile coc-secondary-9L">
								<h3>Advance Voting is
									<br />
									Oct 4-11, 2017 (excluding Oct 9)
								</h3>
								<a class="cui btn-md primary-ghost view-button" data-view-sel="#viewSectionVote_FindAdvanceVote" data-view-title="Find advance voting station" data-event="NavVote" data-comment="Find advance voting station" data-iframe-src="election/Pages/vote/find-advance-vote.aspx?IsDlg=1&iframe=1" data-txt-src="content/vote/vote-find-advance-vote.html.txt">Find advance voting station</a>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="cui tile coc-secondary-9L">
								<h3>Vote on Election Day is
									<br />
									Oct 16, 2017
								</h3>
								<a class="cui btn-md primary-ghost view-button" data-view-sel="#viewSectionVote_FindVoteStation" data-view-title="Find voting station" data-event="NavVote" data-comment="Find voting station" data-iframe-src="election/Pages/vote/find-vote-station.aspx?IsDlg=1&iframe=1" data-txt-src="content/vote/vote-find-vote-station.html.txt">Find voting station</a>
							</div>
						</div>
					</div>
        </section>
        <section id="viewSectionVote_FindAdvanceVote" class="view">
        </section>        
        <section id="viewSectionVote_FindVoteStation" class="view">
        </section>
      </section>
      
      <section id="viewCandidates" class="view">
        <!--header>
          <button class="cui btn-sm primary-ghost view-button" data-view-sel="#viewSectionMayoral_Mayoral" data-view-title="Mayoral" data-event="Candidates" data-comment="Mayoral" data-iframe-src="election/Pages/meet-the-candidates/mayoral/default.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/mayoral.txt">Mayoral</button>
          <button class="cui btn-sm view-button" data-view-sel="#viewSectionCandidates_Councillors" data-view-title="Councillor" data-event="Candidates" data-comment="Councillor" data-iframe-src="election/Pages/meet-the-candidates/councillor/default.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor.txt">Councillors</button>
          <button class="cui btn-sm view-button" data-view-sel="#viewSectionCandidates_School" data-view-title="School Board Trustee" data-event="Candidates" data-comment="School Board Trustee" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/default.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board.txt">School</button>
        </header-->
        <section id="viewSectionCandidates" class="view main-child">
        </section>
      </section>
	  <section id="viewCandidates_Profile" class="view">
        <section id="viewSectionProfile_Profile" class="view main-child zebra-list">
	        <h3>Candidate profile</h3>
        </section>
	  </section>
	  <section id="viewCandidates_Mayoral" class="view">
        <section id="viewSectionMayoral_Mayoral" class="view main-child zebra-list">
        </section>
			</section>
			<section id="viewCandidates_Councillors" class="view">
	    	<!--local navigation menu-->
				<div class="localNavContainer">
					<div class="col-md-12 no-padding">
						<nav class="cui topicNav localNav mobile-view-only" role="navigation">
							<ul class="nav-menu">
								<button type="button" class="navBtnMobile_topic expandBtn" aria-controls="localNav" aria-haspopup="true" aria-label="menu expanded">Councillor candidates menu</button>
								<button type="button" class="navBtnMobile_topic collapseBtn" aria-controls="localNav" aria-haspopup="true" aria-label="menu collapsed" style="display:none;">Councillor candidates menu</button>
								<li class="nav-item active">
									<!--<a href="/election/Pages/candidates/councillors/default.aspx">Councillor Candidates</a>-->
									<ul role="group" aria-expanded="true" aria-hidden="false" class="sub-nav">
										<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward1" data-view-title="Ward 1" data-event="NavCouncillors" data-comment="Ward 1" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-1.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward1.txt">Ward 1</a></li>
										<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward2" data-view-title="Ward 2" data-event="NavCouncillors" data-comment="Ward 2" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-2.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward2.txt">Ward 2</a></li>
										<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward3" data-view-title="Ward 3" data-event="NavCouncillors" data-comment="Ward 3" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-3.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward3.txt">Ward 3</a></li>
										<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward4" data-view-title="Ward 4" data-event="NavCouncillors" data-comment="Ward 4" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-4.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward4.txt">Ward 4</a></li>
										<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward5" data-view-title="Ward 5" data-event="NavCouncillors" data-comment="Ward 5" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-5.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward5.txt">Ward 5</a></li>
										<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward6" data-view-title="Ward 6" data-event="NavCouncillors" data-comment="Ward 6" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-6.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward6.txt">Ward 6</a></li>
										<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward7" data-view-title="Ward 7" data-event="NavCouncillors" data-comment="Ward 7" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-7.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward7.txt">Ward 7</a></li>
										<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward8" data-view-title="Ward 8" data-event="NavCouncillors" data-comment="Ward 8" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-8.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward8.txt">Ward 8</a></li>
										<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward9" data-view-title="Ward 9" data-event="NavCouncillors" data-comment="Ward 9" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-9.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward9.txt">Ward 9</a></li>
										<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward10" data-view-title="Ward 10" data-event="NavCouncillors" data-comment="Ward 10" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-10.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward10.txt">Ward 10</a></li>
										<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward11" data-view-title="Ward 11" data-event="NavCouncillors" data-comment="Ward 11" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-11.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward11.txt">Ward 11</a></li>
										<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward12" data-view-title="Ward 12" data-event="NavCouncillors" data-comment="Ward 12" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-12.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward12.txt">Ward 12</a></li>
										<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward13" data-view-title="Ward 13" data-event="NavCouncillors" data-comment="Ward 13" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-13.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward13.txt">Ward 13</a></li>
										<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward14" data-view-title="Ward 14" data-event="NavCouncillors" data-comment="Ward 14" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-14.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward14.txt">Ward 14</a></li>
									</ul>
								</li>
							</ul>
						</nav>
					</div>
				</div>
      	<!--local nav menu end-->
        <section id="viewSectionCandidates_Councillors" class="view main-child">	
        </section>
		<section id="viewSectionCandidates_Councillors_Ward1" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_Councillors_Ward2" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_Councillors_Ward3" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_Councillors_Ward4" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_Councillors_Ward5" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_Councillors_Ward6" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_Councillors_Ward7" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_Councillors_Ward8" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_Councillors_Ward9" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_Councillors_Ward10" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_Councillors_Ward11" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_Councillors_Ward12" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_Councillors_Ward13" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_Councillors_Ward14" class="view zebra-list">	
        </section>
      </section>
	  <section id="viewCandidates_School" class="view">
	    	<!--local navigation menu-->
				<div class="localNavContainer">
					<div class="col-md-12 no-padding">
						<nav class="cui topicNav localNav mobile-view-only" role="navigation">
							<ul class="nav-menu">
								<button type="button" class="navBtnMobile_topic expandBtn" aria-controls="localNav" aria-haspopup="true" aria-label="menu expanded">School board candidates menu</button>
								<button type="button" class="navBtnMobile_topic collapseBtn" aria-controls="localNav" aria-haspopup="true" aria-label="menu collapsed" style="display:none;">School board candidates menu</button>
								<li class="nav-item active">
											<!--<a href="/election/Pages/candidates/councillors/default.aspx">Councillor Candidates</a>-->
											<ul role="group" aria-expanded="true" aria-hidden="false" class="sub-nav">
												<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_School_Ward1" data-view-title="Ward 1" data-event="NavSchool" data-comment="Ward 1" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/ward-1.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board-ward1.txt">Ward 1</a></li>
												<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_School_Ward2" data-view-title="Ward 2" data-event="NavSchool" data-comment="Ward 2" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/ward-2.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board-ward2.txt">Ward 2</a></li>
												<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_School_Ward3" data-view-title="Ward 3" data-event="NavSchool" data-comment="Ward 3" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/ward-3.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board-ward3.txt">Ward 3</a></li>
												<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_School_Ward4" data-view-title="Ward 4" data-event="NavSchool" data-comment="Ward 4" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/ward-4.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board-ward4.txt">Ward 4</a></li>
												<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_School_Ward5" data-view-title="Ward 5" data-event="NavSchool" data-comment="Ward 5" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/ward-5.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board-ward5.txt">Ward 5</a></li>
												<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_School_Ward6" data-view-title="Ward 6" data-event="NavSchool" data-comment="Ward 6" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/ward-6.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board-ward6.txt">Ward 6</a></li>
												<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_School_Ward7" data-view-title="Ward 7" data-event="NavSchool" data-comment="Ward 7" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/ward-7.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board-ward7.txt">Ward 7</a></li>
												<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_School_Ward8" data-view-title="Ward 8" data-event="NavSchool" data-comment="Ward 8" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/ward-8.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board-ward8.txt">Ward 8</a></li>
												<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_School_Ward9" data-view-title="Ward 9" data-event="NavSchool" data-comment="Ward 9" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/ward-9.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board-ward9.txt">Ward 9</a></li>
												<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_School_Ward10" data-view-title="Ward 10" data-event="NavSchool" data-comment="Ward 10" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/ward-10.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board-ward10.txt">Ward 10</a></li>
												<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_School_Ward11" data-view-title="Ward 11" data-event="NavSchool" data-comment="Ward 11" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/ward-11.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board-ward11.txt">Ward 11</a></li>
												<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_School_Ward12" data-view-title="Ward 12" data-event="NavSchool" data-comment="Ward 12" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/ward-12.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board-ward12.txt">Ward 12</a></li>
												<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_School_Ward13" data-view-title="Ward 13" data-event="NavSchool" data-comment="Ward 13" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/ward-13.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board-ward13.txt">Ward 13</a></li>
												<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_School_Ward14" data-view-title="Ward 14" data-event="NavSchool" data-comment="Ward 14" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/ward-14.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board-ward14.txt">Ward 14</a></li>
												<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_School_AirdrieCandidates" data-view-title="Airdrie Candidates" data-event="NavSchool" data-comment="Airdrie Candidates" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/airdrie-separate-school-candidates.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board-airdrie.txt">Airdrie Candidates</a></li>
												<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_School_ChestermereCandidates" data-view-title="Chestermere Candidates" data-event="NavSchool" data-comment="Chestermere Candidates" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/chestermere-separate-school-candidates.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board-chestermere.txt">Chestermere Candidates</a></li>
												<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_School_CochraneCandidates" data-view-title="Cochrane Candidates" data-event="NavSchool" data-comment="Cochrane Candidates" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/cochrane-separate-school-candidates.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board-cochrane.txt">Cochrane Candidates</a></li>
											</ul>
										</li>
									</ul>
						</nav>
					</div>
				</div>
      	<!--local nav menu end-->
        <section id="viewSectionCandidates_School" class="view main-child">	
        </section>
        <section id="viewSectionCandidates_School_Ward1" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_School_Ward2" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_School_Ward3" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_School_Ward4" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_School_Ward5" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_School_Ward6" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_School_Ward7" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_School_Ward8" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_School_Ward9" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_School_Ward10" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_School_Ward11" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_School_Ward12" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_School_Ward13" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_School_Ward14" class="view zebra-list">	
        </section>
        <section id="viewSectionCandidates_School_AirdrieCandidates" class="view zebra-list">	
		</section>
        <section id="viewSectionCandidates_School_ChestermereCandidates" class="view zebra-list">	
		</section>
        <section id="viewSectionCandidates_School_CochraneCandidates" class="view zebra-list">	
		</section>
      </section>      
      
      <section id="viewUpdate" class="view">
        <section id="viewSectionUpdate_Latest" class="view zebra-list main-child">
        </section>
        <section id="viewSectionUpdate_Article" class="view">
		</section>
      </section>      
      
      <section id="viewContact" class="view">
        <section id="viewSectionContact_Contact" class="view main-child">
					<h1 class="cocis-content-page-title">Contact Us</h1>
					
					<div>
						<p>Need more information on Municipal Elections?<br><br>
							Call <a href="tel:4034764100">403-476-4100</a> (Option 2) or email <a href="mailto:election@calgary.ca">election@calgary.ca</a>.<br><br>
							Mailing address:<br><br>
							Election &amp; Census<br>
							1103 – 55 Avenue NE<br>
							Calgary, AB&nbsp; T2E 6W1
						</p>
					</div>
				</section>
			</section>

      	  
			<!--div id="alertbanner-container" class="coc-pl active">
				<div id="DA3EAEB3-EE2A-46F3-88A3-B7CC9AB8BC0A" class="cui alertbox information" role="alertdialog" aria-labelledby="DA3EAEB3-EE2A-46F3-88A3-B7CC9AB8BC0A-Title" aria-describedby="DA3EAEB3-EE2A-46F3-88A3-B7CC9AB8BC0A-Desc"><div class="alertbanner-box"><div class="alertbanner-box-header"><div class="alert-icon"><span class="cicon cicon-exclamation-circle" aria-hidden="true"></span></div><div class="alert-header"><h2 id="DA3EAEB3-EE2A-46F3-88A3-B7CC9AB8BC0A-Title">Add App to Home Screen</h2></div><div class="alert-closeBtn"><button type="button" role="button" class="closeBtn closeSubNavGroup"><span>close</span><span class="cicon-times cicon"></span></button></div></div><div class="alertbanner-box-contents"><span class="decription-block"><p class="withImageIcon" id="DA3EAEB3-EE2A-46F3-88A3-B7CC9AB8BC0A-Desc">This webpage can be added to your mobile device's home screen with its own icon, without going to the app store.</p><span class="cta-button"><a class="cui btn-sm secondary-ghost homeScreenBtn" role="button" href=""> Add App to Home Screen</a></span></span></div></div></div>
			</div>-->
			<!--<div id="globalInlineAlertNotificationContainer" class="coc-pl active">
				<div id="betaMsg" class="cui label-widget"><span class="cui icon-label-text icon-label-btn-info">Beta <span class="cicon-exclamation-circle" aria-hidden="true"></span></span><p><a href="#betaFeedbackTop">Provide feedback</a> to help enhance the new website navigation.</p></div><div id="76DF7CB2-048B-43D9-98B0-EEAB383E42D3" class="cui alertbox global-inline-alertbox inline-alertbox caution" role="alertdialog" aria-labelledby="76DF7CB2-048B-43D9-98B0-EEAB383E42D3-Title"><div class="alertbanner-box"><div class="alert-icon"><span class="cicon cicon-exclamation-diamond" aria-hidden="true"></span></div><div class="alertbanner-box-contents"><h2 id="76DF7CB2-048B-43D9-98B0-EEAB383E42D3-Title"><span class="boldFont">CAUTION | </span> <a class="cta-btn-global-alert-banner" href="http://www.calgary.ca/Transportation/Roads/Pages/Road-Maintenance/Snow-and-ice-control/Snow-Route-parking-bans.aspx"> See if you are affected by snow route parking bans.</a></h2></div></div></div>
			</div>-->
      
      <!-- /Body (end) -->
    </main>
    
    <!-- build:js scripts/vendor.min.js -->
	<script src="libs/es6-polyfill/polyfill.js"></script><!-- used by COC.SPList.js for desktop IE compatibility until we use babel + babel promises polyfil to compile all js -->
	<script src="libs/jquery/jquery-2.2.4/jquery-2.2.4.js"></script>
	<script src="libs/jquery-ui-1.12.1.custom.accordion-only/jquery-ui.min.js"></script><!--jquery-ui autocomplete used by find my ward -->
	<script src="libs/addtohomescreen/addtohomescreen.custom.js?v1.145"></script>
	<script src="libs/geodesy/latlon.js"></script>
	<!-- endbuild -->
	
    <!-- build:js scripts/app.min.js -->
    <script src="js/COC.Nav.js?v1.145"></script>
    <script src="js/COC.SPList.js?v1.1453"></script>
    <script src="js/COC.Election.js?v1.146"></script><!-- Should be a copy of any updates to COC.Election.js used on the website version - Used by find my ward and other functionality -->
	<!-- load after initializing "add to homescreen" plugin which looks for ? querystring that gets added when app loaded from homescreen as a PWA, to prevent "add to homescreen" popup from appearing if you loaded from homescreen -->
    <script src="js/elections.js?v1.1451"></script>
	<script src="js/app.utils.js?v1.1451"></script>
    <script src="js/app.js?v1.1456"></script>
	<!-- endbuild -->

	</body>
</html>

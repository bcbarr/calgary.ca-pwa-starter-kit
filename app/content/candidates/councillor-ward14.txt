﻿<a class="mobile-back-btn" href="Default.aspx#"><button class="cui btn-md secondary-ghost">Go back</button></a>

<div class="subtitle-block cui election-subtitle-block">
	<h2>Councillor Candidates for Ward 14</h2>
	<hr />
</div>

<div>	
	<section class="content-placeholder candidates-Councillor ward1">
	    <div class="row">

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Peter_Demong-Ward14.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Peter Demong</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=PeterDemong">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>
	
	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square"></div>
	          <div class="content-container">
	              <h2>Kelash Kumar</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=KelashKumar">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

	   </div>
	</section>
</div>
<a class="cui btn-md secondary-ghost view-button" role="button" data-view-sel="#viewSectionCandidates_School_Ward14" data-view-title="Ward 14" data-event="Content" data-comment="Ward 14" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/ward-14.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board-ward14.txt">View school board candidates</a>
﻿<a class="mobile-back-btn" href="Default.aspx#"><button class="cui btn-md secondary-ghost">Go back</button></a>

<div class="subtitle-block cui election-subtitle-block">
	<h2>Councillor Candidates for Ward 2</h2>
	<hr />
</div>

<div>	
	<section class="content-placeholder candidates-Councillor ward1">
	    <div class="row">

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	            <div class="image-placeholder square" 
				style="background:url(/election/PublishingImages/Photos/George_Georgeou-Ward2.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>George Georgeou</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=GeorgeGeorgeou">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" 
	          style="background:url(/election/PublishingImages/Photos/JoeMagliocca-Ward2.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Joe Magliocca</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=JoeMagliocca">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" 
			  style="background:url(/election/PublishingImages/Photos/Christopher_Maitland_Ward2.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Christopher Maitland</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=ChristopherMaitland">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>
	      
	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Jennifer_Wyness-Ward2.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Jennifer Wyness</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=JenniferWyness">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

	   </div>
	</section>
</div>
<a class="cui btn-md secondary-ghost view-button" role="button" data-view-sel="#viewSectionCandidates_School_Ward2" data-view-title="Ward 2" data-event="Content" data-comment="Ward 2" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/ward-2.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board-ward2.txt">View school board candidates</a>
﻿<a class="mobile-back-btn" href="Default.aspx#"><button class="cui btn-md secondary-ghost">Go back</button></a>

<div class="subtitle-block cui election-subtitle-block">
	<h2>Councillor Candidates for Ward 4</h2>
	<hr />
</div>

<div>	
	<section class="content-placeholder candidates-Councillor ward1">
	    <div class="row">

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Blair_Berdusco-Ward4.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Blair Berdusco</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=BlairBerdusco">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Sean_Chu-Ward4.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Sean Chu</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=SeanChu">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Srinivas_Ganti_Ward4.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Srinivas Ganti</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=SrinivasGanti">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Greg_Miller-Ward4.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Greg Miller</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=GregMiller">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

	   </div>
	</section>
</div>
<a class="cui btn-md secondary-ghost view-button" role="button" data-view-sel="#viewSectionCandidates_School_Ward4" data-view-title="Ward 4" data-event="Content" data-comment="Ward 4" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/ward-4.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board-ward4.txt">View school board candidates</a>
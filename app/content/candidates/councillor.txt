﻿<div id="councillorCandidatesFindMyWard" class="ele-right-page-Content">

	<div class="subtitle-block cui election-subtitle-block">
		<h2>Councillor candidates</h2>
		<hr>
	</div>

	<!--Label widget - with text-->
	<div class="cui label-widget">
		<span class="cui icon-label-text icon-label-btn-info">
		    New   
		    <span class="cicon-info-circle" aria-hidden="true"></span>
		</span>
		<p><a class="view-button" data-view-sel="#viewSectionVoterInfo_WardBoundaryChanges" data-view-title="Ward Boundary Changes" data-event="Content" data-comment="Ward Boundary Changes" data-iframe-src="election/Pages/information-for-voters/ward-boundary-changes.aspx?IsDlg=1&iframe=1" data-txt-src="content/voterInfo/ward-boundary-changes.html.txt">Ward boundaries are changing</a> this 2017 election year.</p>
	</div>

	<p>&nbsp;</p>
	
	<ul role="group" aria-expanded="true" aria-hidden="false" class="sub-nav">
		<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward1" data-view-title="Ward 1" data-event="Content" data-comment="Ward 1" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-1.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward1.txt">Ward 1</a></li>
		<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward2" data-view-title="Ward 2" data-event="Content" data-comment="Ward 2" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-2.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward2.txt">Ward 2</a></li>
		<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward3" data-view-title="Ward 3" data-event="Content" data-comment="Ward 3" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-3.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward3.txt">Ward 3</a></li>
		<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward4" data-view-title="Ward 4" data-event="Content" data-comment="Ward 4" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-4.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward4.txt">Ward 4</a></li>
		<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward5" data-view-title="Ward 5" data-event="Content" data-comment="Ward 5" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-5.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward5.txt">Ward 5</a></li>
		<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward6" data-view-title="Ward 6" data-event="Content" data-comment="Ward 6" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-6.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward6.txt">Ward 6</a></li>
		<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward7" data-view-title="Ward 7" data-event="Content" data-comment="Ward 7" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-7.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward7.txt">Ward 7</a></li>
		<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward8" data-view-title="Ward 8" data-event="Content" data-comment="Ward 8" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-8.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward8.txt">Ward 8</a></li>
		<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward9" data-view-title="Ward 9" data-event="Content" data-comment="Ward 9" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-9.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward9.txt">Ward 9</a></li>
		<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward10" data-view-title="Ward 10" data-event="Content" data-comment="Ward 10" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-10.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward10.txt">Ward 10</a></li>
		<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward11" data-view-title="Ward 11" data-event="Content" data-comment="Ward 11" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-11.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward11.txt">Ward 11</a></li>
		<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward12" data-view-title="Ward 12" data-event="Content" data-comment="Ward 12" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-12.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward12.txt">Ward 12</a></li>
		<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward13" data-view-title="Ward 13" data-event="Content" data-comment="Ward 13" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-13.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward13.txt">Ward 13</a></li>
		<li class=""> <a class="view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward14" data-view-title="Ward 14" data-event="Content" data-comment="Ward 14" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-14.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward14.txt">Ward 14</a></li>
	</ul>
    
    <!--	
    <section class="find-ward">
	  	<div class="find-ward-info content-placeholder">
			<p>To find your ward, enter your home address below</p>
			<span>Home address:</span>
			<div class="row">
				<div class="col-md-6">
					<label for="searchHomeAddress" class="cocis-hidden-text">Search field</label>				
					<input class="searchHomeAddress" type="search" aria-labelledby="searchHomeAddress" placeholder="House number and street name" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" />
				</div>
				<div class="col-md-6">
					<a href="" class="btnCheckAddress cui btn-sm secondary-ghost" role="button">Check ward</a>
				</div>
			</div>
			<p>E.g. 1103 - 55 Avenue NE - Do not use '#' in address - Do not use suite or unit numbers</p>
			<p class="divWardNum" style="display:none;"></p>
			<p>OR use the map to find your home.</p>
			<p>Having trouble locating your ward by home address? You could try referring to a <a class="view-button" data-view-sel="#viewSectionVoterInfo_NewWardBoundaries" data-view-title="New Ward Boundaries" data-event="Content" data-comment="New Ward Boundaries" data-iframe-src="election/Pages/information-for-voters/new-ward-boundaries-oct-16.aspx?IsDlg=1&iframe=1" data-txt-src="content/voterInfo/new-ward-boundaries-oct-16.html.txt">table of wards by community</a> to find your ward or, <a class="view-button" data-view-sel="#viewContact" data-view-title="Contact Us" data-event="Content" data-comment="Contact Us">contact us</a>.</p>
		</div>
		<script type="text/javascript">
			COC.Election.FindWard.Init( $('#councillorCandidatesFindMyWard'), true );
		</script>
	</section>
	<section>
		<style>
			.embed-container { margin-top:18px; border:1px solid black; position: relative; padding-bottom: 100%; height: 0; max-width: 100%;} .embed-container iframe, .embed-container object, .embed-container iframe{position: absolute; top: 0; left: 0; width: 100%; height: 100%;} small{position: absolute; z-index: 40; bottom: 0; margin-bottom: -15px;}
		</style>
		<div class="embed-container">
			<iframe class="iFrameWardMap" width="500" height="400" src="" frameborder="0">
			</iframe>
		</div>
		<script>
			COC.Election.FindWard.InitMap( $('#councillorCandidatesFindMyWard') );
		</script>
	</section>
	-->

</div>
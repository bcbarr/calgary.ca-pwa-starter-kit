﻿<a class="mobile-back-btn" href="Default.aspx#"><button class="cui btn-md secondary-ghost">Go back</button></a>

<h1>School Board Candidates for Ward 10</h1>

<section class="content-placeholder public-school-board-candidates ward1">
  	<div class="subtitle-block cui election-subtitle-block">
  		<h2>Public school trustee candidates for Ward 10</h2>
  		<hr>
  	</div>

    <div class="row">
    
	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/JasbirChahal_PubSchool5-10_2.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Jasbir (Jas) Chahal</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=JasbirChahal">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Marilyn_Dennis-PublicSchoolTrusteeWards5-10.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Marilyn Dennis</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=MarilynDennis">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

  	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Rekha_Dhawan-PublicSTWards5and10.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Rekha Dhawan</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=RekhaDhawan">View Profile</a></li>
	          </ul>    
	        </div>
	      </div> 
				
	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Jameela_Ghann_SchoolTrustee.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Jameela Ghann</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=JameelaGhann">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Raman_Dill_PublicSchoolTrusteeWards5-10.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Raman Gill</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=RamanGill">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Gurpreat_Grewal-PublicSTWards5-10.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Gurpreet Grewal</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=GurpreetGrewal">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Syed_Hasnain_Public_School_Trustee_Wards_5and10.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Syed Hasnain</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=SyedHasnain">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Pamela_King-PublicSchoolTrusteeWards5-10.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Pamela King</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=PamelaKing">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	               <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Sohail_Sherwani-PublicSchoolTrusteeWards5-10.jpg) center no-repeat !important; background-size: cover !important;"></div> 
	          <div class="content-container">
	              <h2>Sohail Sherwani</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=SohailSherwani">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>
    
	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Bianca_Smetacek-PublicSchoolTrustee_Wards5-10.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Bianca Smetacek</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=BiancaSmetacek">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square"></div>
	          <div class="content-container">
	              <h2>Veerpal Rai</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=VeerpalRai">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Wasima_Sultan-PublicSchoolTrusteeWards5-10.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Wasima Sultan</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=WasimaSultan">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

    </div>

</section>

<section class="content-placeholder separate-school-board-candidates ward1">
  	<div class="subtitle-block cui election-subtitle-block">
  		<h2>Separate school trustee candidates for Ward 10</h2>
  		<hr>
  	</div>

    <div class="row">

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Mario_Deshaies-SepareteSchoolTrusteeWards9_10_Chestermere.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Mario Deshaies</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=MarioDeshaies">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Cheryl_Low-SeparateSchoolTrusteeWards9-10-Chestermere.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Cheryl Low</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=CherylLow">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

    </div>

</section>

<a class="cui btn-md secondary-ghost view-button" role="button" data-view-sel="#viewSectionCandidates_Councillors_Ward10" data-view-title="Ward 10" data-event="Content" data-comment="Ward 10" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-10.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward10.txt">View councillor candidates</a>
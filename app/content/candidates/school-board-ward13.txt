﻿<a class="mobile-back-btn" href="Default.aspx#"><button class="cui btn-md secondary-ghost">Go back</button></a>

<h1>School Board Candidates for Ward 13</h1>

<section class="content-placeholder public-school-board-candidates ward1">
  	<div class="subtitle-block cui election-subtitle-block">
  		<h2>Public school trustee candidates for Ward 13</h2>
  		<hr>
  	</div>

    <div class="row">

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Julie_Hrdlicka-PublicSchoolTrusteeWards11-13.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Julie Hrdlicka</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=JulieHrdlicka">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>
	      
	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Sadiq_Valliani-PubilcSchoolTrusteeWards11-13.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Sadiq Valliani</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=SadiqValliani">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

    </div>

</section>

<section class="content-placeholder separate-school-board-candidates ward1">
  	<div class="subtitle-block cui election-subtitle-block">
  		<h2>Separate school trustee candidates for Ward 13</h2>
  		<hr>
  	</div>

    <div class="row">

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Antonio_Balangue_SeparateSchoolTrustee-Wards13-14.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Antonio Balangue</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=AntonioBalangue">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>
	      
	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Mary_Martin_SeperateSchoolTrusteeWards13-14.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Mary Martin</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=MaryMartin">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

    </div>

</section>

<a class="cui btn-md secondary-ghost view-button" role="button" data-view-sel="#viewSectionCandidates_Councillors_Ward13" data-view-title="Ward 13" data-event="Content" data-comment="Ward 13" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-13.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward13.txt">View councillor candidates</a>
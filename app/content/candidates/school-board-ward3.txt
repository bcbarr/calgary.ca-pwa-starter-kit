﻿<a class="mobile-back-btn" href="Default.aspx#"><button class="cui btn-md secondary-ghost">Go back</button></a>

<h1>School Board Candidates for Ward 3</h1>

<section class="content-placeholder public-school-board-candidates ward1">
  	<div class="subtitle-block cui election-subtitle-block">
  		<h2>Public school trustee candidates for Ward 3</h2>
  		<hr>
  	</div>

    <div class="row">

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Althea_Adams-PublicSchoolTrusteeWards3-4.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Althea Adams</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=AltheaAdams">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Nimra_Amjad-PublicSchoolTrusteeWards3-4.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Nimra Amjad</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=NimraAmjad">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>
	      
	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Laura_Hack-PublicSchoolTrusteeWards3-4.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Laura Hack</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=LauraHack">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Kim_Tyers-PublicSchoolTrusteeWards3-4.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Kim Tyers</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=KimTyers">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

    </div>
</section>

<section class="content-placeholder separate-school-board-candidates ward1">
  	<div class="subtitle-block cui election-subtitle-block">
  		<h2>Separate school trustee candidates for Ward 3</h2>
  		<hr>
  	</div>

    <div class="row">
    
	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Linda_Wellman-SeparateSchoolTrusteeWards3-5-Airdrie.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Linda Wellman - Acclaimed</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=LindaWellman">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>
	      
    </div>

</section>

<a class="cui btn-md secondary-ghost view-button" role="button" data-view-sel="#viewSectionCandidates_Councillors_Ward3" data-view-title="Ward 3" data-event="Content" data-comment="Ward 3" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-3.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward3.txt">View councillor candidates</a>
﻿<a class="mobile-back-btn"  href="Default.aspx">button class="cui btn-md secondary-ghost">Go back</button></a>

<h1>School Board Candidates for Ward 8</h1>

<section class="content-placeholder public-school-board-candidates ward1">
  	<div class="subtitle-block cui election-subtitle-block">
  		<h2>Public school trustee candidates for Ward 8</h2>
  		<hr>
  	</div>

    <div class="row">

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Sabrina_Bartlett-PublicSchoolTrusteeWards8-9.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Sabrina Bartlett</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=SabrinaBartlett">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Richard_Hehr-PublicSchoolTrusteeWards8-9.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Richard Hehr</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=RichardHehr">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Merle_Terlesky-PublicSchoolTrusteeWards8-9.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Merle Terlesky</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=MerleTerlesky">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

    </div>

</section>

<section class="content-placeholder separate-school-board-candidates ward1">
  	<div class="subtitle-block cui election-subtitle-block">
  		<h2>Separate school trustee candidates for Ward 8</h2>
  		<hr>
  	</div>

    <div class="row">

	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square" style="background:url(/election/PublishingImages/Photos/Lory_Iovinelli-SeparateSchoolTrusteeWards6-8.jpg) center no-repeat !important; background-size: cover !important;"></div>
	          <div class="content-container">
	              <h2>Lory Iovinelli</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=LoryIovinelli">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>
	      
	      <div class="col-md-3">
	        <div class="cui card list-cta mayor-1">
	          <div class="image-placeholder square"></div>
	          <div class="content-container">
	              <h2>Christopher McMillan</h2>
	          </div>
	          <ul class="action-links">
	            <li><a class="view-button" data-view-sel="#viewCandidates_Profile" data-view-title="Candidate Profile" data-event="Candidates" data-comment="Candidate Profile" data-txt-src="content/candidates/profile.txt?profileId=ChristopherMcMillan">View Profile</a></li>
	          </ul>    
	        </div>
	      </div>

    </div>

</section>

<a class="cui btn-md secondary-ghost view-button" role="button" data-view-sel="#viewSectionCandidates_Councillors_Ward8" data-view-title="Ward 8" data-event="Content" data-comment="Ward 8" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-8.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward8.txt">View councillor candidates</a>
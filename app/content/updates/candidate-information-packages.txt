﻿<h1 class="cocis-content-page-title">Candidate Information Packages are ready</h1>

<a class="mobile-back-btn" onclick="javascript:window.history.back();return false;" href="Default.aspx#"><button class="cui btn-md secondary-ghost">Go back</button></a>

<div>
	<p>If you are planning on running for office in the next municipal election, you will need to pick-up your Nomination Papers. A Candidate Information Handbook and Election Bylaws are also available. These documents will provide you with important information about legislation, bylaws and policies.</p>
	<p>Printed copies are now available for pick-up at the Elections Calgary office (1103 – 55 Avenue NE, Calgary, AB).</p>
	<p>Digital copies of the Candidate Information Handbook and Election Bylaws are available <a class="external-link" data-event="NavVoterInfo" href="/election/Pages/information-for-candidates/default.aspx" target="_blank">online <span class="cicon-external-link"></span></a>.</p>
	<p>Nomination Papers must be picked up in-person by the candidate or a representative. Nomination Papers cannot be reproduced, altered or disassembled.</p>
</div>
﻿<h1 class="cocis-content-page-title">Candidate Information Session</h1>

<a class="mobile-back-btn" onclick="javascript:window.history.back();return false;" href="Default.aspx#"><button class="cui btn-md secondary-ghost">Go back</button></a>

<div><p>The Calgary Board of Education Board of Trustees is hosting a <a class="external-link" data-event="Content" href="http://www.cbe.ab.ca/about-us/board-of-trustees/Pages/Default.aspx" target="_blank">Candidate Information Session <span class="cicon-external-link"></span></a> on June 13, 2017 from 6:30 p.m. – 8:30 p.m. to learn more about being a trustee. The session will be held in the Multi-Purpose Room of the Education Centre at <a class="external-link" data-event="Content" href="https://www.google.ca/maps/place/Calgary+Board+Of+Education+-+Corporate/%4051.041729%2c-114.08184%2c17z/data=%214m5%211m2%212m1%211scalgary+board+of+education+-+1221+8+street+s.w.%213m1%211s0x0000000000000000:0x204dda7040bd79f8" target="_blank">1221 – 8 Street S.W. <span class="cicon-external-link"></span></a>, Calgary, Alberta</p>
<p>For additional information on the information session or to RSVP your attendance, please contact the Board of Trustee Office:</p>

<p>Phone | <a>403-817-7933</a></p>
<p>Email | <a href="mailto:BoardofTrustees@cbe.ab.ca">BoardofTrustees@cbe.ab.ca</a></p>
</div>
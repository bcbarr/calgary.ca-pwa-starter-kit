﻿<h1 class="cocis-content-page-title">Who can vote?</h1>

<div class="container-fluid" unselectable="on">
  <div class="row-fluid" unselectable="on">
    <div class="col-md-8" unselectable="off">

<p>
You are eligible to vote in the 2017 Calgary General  Election if on Election Day (October 16, 2017), you:
</p>
<ul>
  <li>Are at least 18 year old; and</li>
  <li>Are a Canadian citizen; and</li>
  <li>Have resided in Alberta since April 16, 2017;  and</li>
  <li>Are a resident on Election Day;</li>
  <ul>
    <li>Of the city of Calgary, to vote&nbsp;for Mayor; and</li>
    <li>Of the ward, to vote&nbsp;for Councillor; and</li>
<li>Of the Calgary Board of Education, to vote for
Public School Trustee; or</li>
<li>Of the Calgary Roman Catholic Separate School District No. 1, to vote for Separate School Trustee​<br></li>
  </ul>
  <li>Provide one of the authorized pieces of <a class="view-button" data-view-sel="#viewSectionVoterInfo_IdentificationRequirements" data-view-title="Identification Requirements" data-event="Content" data-comment="Identification Requirements" data-iframe-src="election/Pages/information-for-voters/identification-requirements.aspx?IsDlg=1&iframe=1" data-txt-src="content/voterInfo/identification-requirements.html.txt">identification</a>.</li>
</ul>
<p>Before you vote you must sign a statement that you are  eligible to vote.<em>*</em><br><br> *<em> It is an offense to  sign a false statement.<strong></strong></em></p>



<h3><strong>Students</strong></h3>
A student who:

<ul>
  <li>Attends an educational institution within or,  outside Alberta;</li>
  <li>Temporarily rents accommodation for the purpose  of attending an educational institution; and</li>
  <li>Has family members who are resident in Alberta and with whom the student ordinarily resides when not attending an educational institution, is deemed to be a resident.​<br></li>
</ul>



</div>
    <div class="col-md-4" unselectable="off"><img class="cocis-image" src="election/PublishingImages/voting%20compartment2.jpg" border="0" alt="" style="padding:0px">&nbsp;</div>
  </div>
</div>
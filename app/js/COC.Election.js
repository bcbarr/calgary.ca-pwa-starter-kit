﻿var COC = COC || {};
COC.Election = COC.Election || {};
COC.Election.PageStates = {
	ShowCandidates: function() {
		$('body').addClass('ele-show-candidates-on');
	},
	ShowFindMyWard: function() {
		$('body').addClass('ele-find-my-ward-on');
	},
};
//NOTE: These HtmlTemplates get overridden in elections web app in elections.js
COC.Election.HtmlTemplates = {
	WardTableFindMyWard: function(strWard) {
		return '. You will be in either <span style="font-weight:bold;">Ward ' + strWard + '</span>. Please find your exact ward by address.<br /><a href="/election/Pages/information-for-voters/find-my-ward.aspx" class="cui btn-lg secondary">Find my Ward</a>';
	},
	WardTableReviewCandidates: function(strWard) {
		return ' will be in <span style="font-weight:bold;">Ward ' + strWard + '</span>. Please review your candidates.<br /><a href="/election/Pages/meet-the-candidates/councillor/ward-'+strWard+'.aspx" class="cui btn-lg secondary">Councillor Candidates</a> <a href="/election/Pages/meet-the-candidates/school-trustee/ward-'+strWard+'.aspx" class="cui btn-lg secondary">School Board Candidates</a>';
	},
	FindMyWardLookupResult: function(strWard) {
		return ' Please review your candidates.<p><a href="/election/Pages/meet-the-candidates/councillor/ward-'+strWard+'.aspx" class="cui btn-lg secondary">Councillor Candidates</a> <a href="/election/Pages/meet-the-candidates/school-trustee/ward-'+strWard+'.aspx" class="cui btn-lg secondary">School Board Candidates</a></p>';	
	},
	ProfileBtnBackToMayoral: function() {
		return '<a class="cui btn-lg secondary" href="/election/Pages/meet-the-candidates/mayoral/default.aspx">&lt; Candidates</a>';
	},
	ProfileBtnBackToCouncillorWard: function(strWard) {
		return '<a class="cui btn-lg secondary" href="/election/Pages/meet-the-candidates/councillor/ward-'+strWard+'.aspx">&lt; Candidates for Ward '+strWard+'</a>';
	},
	ProfileBtnBackToSchoolWard: function(strWard, strWard2) {
		return '<a class="cui btn-lg secondary" href="/election/Pages/meet-the-candidates/school-trustee/ward-'+strWard+'.aspx">&lt; Candidates for Ward '+strWard+'</a><br /><a class="cui btn-lg secondary" href="/election/Pages/meet-the-candidates/school-trustee/ward-'+strWard2+'.aspx">&lt; Candidates for Ward '+strWard2+'</a>';
	},
	ProfileBtnBackToOtherSeparateSchool: function() {
		return '<a class="cui btn-lg secondary" href="/election/Pages/meet-the-candidates/school-trustee/other-separate-school-candidates.aspx">&lt; Candidates</a>';
	},
};
COC.Election.MobileBanner = {
	Init: function() {
		COC.Election.MobileBanner.CheckIfMobileDevice();
	},
	CheckIfMobileDevice: function() {
		// User Agent check adapted from http://detectmobilebrowsers.com > Javascript - e.g. of use case is election 2017 website home page to show election mobile web app cta banner if browsing from mobile device
		checkIfMobile = function() {
		    var userAgent = navigator.userAgent||navigator.vendor||window.opera;
		    return (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(userAgent)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(userAgent.substr(0,4)));
		};
		if(checkIfMobile()) {
			$('#mobileCTABanner').show();
		}
	},
};
COC.Election.WardList = {
	Init: function() {
		$('#tblWardList tr').one('click', 'a[href="#"]', COC.Election.WardList.ClickCommunity);
	},
	ClickCommunity: function(e) {
		if($(this).attr('href') !== '#')
			return;
		var $tr = $(this).closest('tr');
		var $td = $tr.find('td:eq(1)');
		$(this).attr('href','javascript:void(0);');
		// determine path based on ward(s) listed
		var strWard = $td.next('td').text().trim().replace(' (new)','');
		if(strWard.search(' or ') > -1) {
			// You could be in more than one ward. Please search for your address to verify which ward you will be voting under this election year so that you can review the correct candidate choices.
			//$td.append('. You will be in either <span style="font-weight:bold;">Ward ' + strWard + '</span>. Please find your exact ward by address.<br /><a href="/election/Pages/meet-the-candidates/councillor/default.aspx" class="cui btn-lg secondary">Find my Ward</a>');
			$td.append( COC.Election.HtmlTemplates.WardTableFindMyWard(strWard) );
		} else {
			// You are in Ward #. Please review candidates.
			//$td.append(' will be in <span style="font-weight:bold;">Ward ' + strWard + '</span>. Please review your candidates.<br /><a href="/election/Pages/meet-the-candidates/councillor/ward-'+strWard+'.aspx" class="cui btn-lg secondary">Councillor Candidates</a> <a href="/election/Pages/meet-the-candidates/school-trustee/ward-'+strWard+'.aspx" class="cui btn-lg secondary">School Board Candidates</a>');
			$td.append( COC.Election.HtmlTemplates.WardTableReviewCandidates(strWard) );
		}
		// if elections web app (ie: elections.js loaded) then also initialize click events for the dynamically injected buttons:
		if(typeof Elect !== 'undefined' && Elect.ViewSectionsFromTxt && Elect.ViewNav) {
			Elect.ViewSectionsFromTxt.initViewBtnClickEvents($tr);
			Elect.ViewNav.initHtmlFromTxtEvents($tr);
		}

		e.preventDefault();
		return false;
	},
};
COC.Election.FindWard = {
	Init: function($viewContainer, isShowingCandidates /*optional*/) {
		var isShowingCandidates = (typeof isShowingCandidates !== 'undefined') ? isShowingCandidates : true;  //show the candidate link buttons by default
		$viewContainer.find('.searchHomeAddress').keydown(function(e) {
			if(e.keyCode === 13){
				$viewContainer.find('.btnCheckAddress').trigger('click');
				e.preventDefault();
				return false;
			}
		});
		$viewContainer.find('.btnCheckAddress').click(function(e) {
			if($viewContainer.selector === '#findVoteStation')
				COC.Election.VoteStn.reset();
		
			var strHomeAddress = $viewContainer.find('.searchHomeAddress').val().trim();
			if(strHomeAddress == '') {
				$viewContainer.find('.divWardNum').show().html('Address cannot be blank');
				COC.Analytics.wt({'name':'Check Ward Button','comment':'No Address Provided'});
			} else {
				$viewContainer.find('.divWardNum').show().html('Searching for address...');
				var encodedAddress = encodeURIComponent(strHomeAddress);

				//New strategy:
				//When user entered in an address in our address lookup tool and hit enter it used to succeed on "800 Macleod Tr SE", fail on "800 Macleod Trail SE", and succeed on an invalid address like "800 Macleod Tr SE 102"
				//Now when user enters an address and hits enter it should success on "800 Macleod Trail SE" and fail on "800 Macleod Tr SE 102"
				//We use one web service to autosuggest and address. But then we use a different tool to get "address candidates" upon hitting enter and if an address is returned then it will use that one instead of what the user entered. We will change the user's input though (CAI did not do that) so you know what it evaluates to in case it evaluates incorrectly.
				//FIND ADDRESS CANDIDATES AND TRY FIRST ONE IN LIST
				
				//CONSIDER: First, like the CAI map we should verify that user typed in house number, street name, street type and quadrant before allowing search... otherwise it will just go ahead and search.
				//But we are going to try this indead: Let user search "early" and then change value of address input field to match what was searched.

				//Close autocomplete upon clicking enter:
				$viewContainer.find('.searchHomeAddress').autocomplete('close');
				
				$.ajax({
					//NOTE: This service is not the same, more forgiving lookup used by autocomplete - this service is the same one used by CAI in the new 2017 ward boundaries map when you search on an address and it is much more strict with requiring a valid address
					url: "https://gis.calgary.ca/arcgis/rest/services/pub_LocatorsAGOL/Calgary_VotingAddress_Locator/GeocodeServer/findAddressCandidates?Single%20Line%20Input=" + encodedAddress + "&f=json&outSR=%7B%22wkid%22%3A102100%7D&outFields=*&maxLocations=6",
					type:"GET",
					dataType: "json",
					success: function( data, status ) {
						//if address candidate(s) returned then use the first one to appear. This helps rewrite address input in to "City of Calgary address format" if it was close (e.g. rewrites "800 Macleod Trail SE" to "800 Macleod Tr SE")
						if(data && data.candidates && data.candidates.length > 0) {
							var addressCandidate = data.candidates[0].address;
							COC.Election.FindWard.ChangeAddressInputVal($viewContainer, addressCandidate);
							COC.Election.FindWard.LoadMap($viewContainer, encodeURIComponent(addressCandidate), isShowingCandidates);
						} else {
							// No "Voting address location" matched the address entered - display an error
							COC.Analytics.wt({'name':'Check Ward Button','comment':'Invalid Address'});
							$viewContainer.find('.divWardNum').show().html('Invalid address');
							
				        	var response = '<p><span style="font-weight:bold;">Address could not be found</span></p>';
				        	response += '<p>Instructions:</p><ol style="margin-bottom: 0;"><li>Do not use suite or unit numbers.</li><li>Enter in your house number/building number and street name. E.g: 1103 - 55.</li><li>Wait for a list of addresses to be suggested.</li><li>Find your home address in that list and click on it</li><li>Click on "Check ward" button.</li></ol>';
					        $viewContainer.find('.divWardNum').show().html(response);
						}
					}
				});
				
			}
			e.preventDefault();
			return false;
		});
		COC.Election.FindWard.InitAddressAutocomplete($viewContainer);
		
		if($viewContainer.selector === '#findVoteStation')
			COC.Election.VoteStn.reset();
	},
	ChangeAddressInputVal: function($viewContainer, val) {
		//Prevents autocomplete from appearing when we manually update val in address field - only handles this event one time
		$viewContainer.find('.searchHomeAddress').one('change', function(e) {
			e.preventDefault();
			return false;
		});							
		//Put address value actually being used in to the address input area, which normally triggers autocomplete to appear
		$viewContainer.find('.searchHomeAddress').val(val);
	},
	//What changed from old address autocomplete:
	//1. Using same service as the gis map located here: https://thecityofcalgary.maps.arcgis.com/apps/webappviewer/index.html?id=5280281b1d85494180dbfdc8497d2c9a  which is: "https://pigisst0.coc.ca/arcgis/rest/services/pub_LocatorsAGOL/Calgary_VotingAddress_Locator/GeocodeServer/findAddressCandidates?Single%20Line%20Input=" + request.term + "&f=json&outSR=%7B%22wkid%22%3A102100%7D&outFields=*&maxLocations=6" instead of the old url: "https://gis.calgary.ca/arcgis/rest/services/pub_Locators/CalgaryUniversalLocator/GeocodeServer/findAddressCandidates?Address=" + request.term + "&f=json&outSR=%7B%22wkid%22%3A102100%7D&outFields=Loc_name%2C%20Score%2C%20Match_addr&searchExtent=%7B%22xmin%22%3A-114.35553566841509%2C%22ymin%22%3A50.751760821086485%2C%22xmax%22%3A-113.81654649794336%2C%22ymax%22%3A51.31666270821702%2C%22spatialReference%22%3A%7B%22wkid%22%3A4326%7D%7D",
	//2. But borrowing from CAI's strategy: Automatically go with the first item that appears in the autocomplete list if user hits enter.
	InitAddressAutocomplete: function($viewContainer) {
	
		//New strategy:
		//Now when user types we will only request a maximum of 1 autocomplete suggestion that the user can choose from (added maxLocations=1 querystring parameter to the request to limit the response).
		var maxAutocompleteResults = 1;
		
		// setup address autocomplete
		$viewContainer.find('.searchHomeAddress').autocomplete({
			source: function( request, response ) {
				$.ajax({
					url: "https://gis.calgary.ca/arcgis/rest/services/pub_Locators/CalgaryUniversalLocator/GeocodeServer/findAddressCandidates?Address=" + request.term + "&f=json&outSR=%7B%22wkid%22%3A102100%7D&outFields=Loc_name%2C%20Score%2C%20Match_addr&maxLocations=1&searchExtent=%7B%22xmin%22%3A-114.35553566841509%2C%22ymin%22%3A50.751760821086485%2C%22xmax%22%3A-113.81654649794336%2C%22ymax%22%3A51.31666270821702%2C%22spatialReference%22%3A%7B%22wkid%22%3A4326%7D%7D",
					type:"GET",
					dataType: "json",
					success: function( data, status ) {					        
						var data = $(data.candidates).map(function( idx, item ) {
							if (item.location.x != "NaN") {
								return {
									label: item.address,
									value: item.address
									//value: item.location.x + "," + item.location.y
								}
							}
						});
						var intRegex = /^\d+$/;
						var firstChar = request.term.substring(0,1);
						if (request.term.length > 2) {
							if (intRegex.test(firstChar)) {
								if(!data.length) {
									if(request.term.match(/ /g).length > 1) {  // NOTE: Added because system can only suggest an address after the street name has been typed (e.g. "800 Macleod"), so this code lets system begin searching after 2 spaces have been typed (e.g. "800 Macleod ") otherwise, it would say "No address found" until the street name has been fully entered.
										var result = [
										{
											label: 'Enter in your house number/building number and street name',
											value: response.term
										}];
										response(result);
									}
								} else {    
									response(data);
								}  	 
							} else {
								var result = [{
										label: 'Address must begin with a house/unit number',
										value: response.term
								}];
								response(result);
							} 
						}
					 }
				});
			},
			minLength: 3,
			delay: 500,
			open: function() {
				$(this).removeClass('ui-corner-all').addClass('ui-corner-top');
			},
			close: function() {
				$(this).removeClass('ui-corner-top').addClass('ui-corner-all');
			}
		});
	},
	InitMap: function($viewContainer) {
		COC.Election.FindWard.LoadMapNoAddress($viewContainer);
	},
	LoadMapNoAddress: function($viewContainer) {
		var mapurl = "https://thecityofcalgary.maps.arcgis.com/apps/Embed/index.html?webmap=d51ca635db74438dbcb7883640e6f292&amp;home=true&amp;zoom=true&amp;scale=true&amp;search=true&amp;searchextent=true&amp;legendlayers=true&amp;basemap_gallery=true&amp;disable_scroll=false&amp;theme=light";
		$viewContainer.find('.iFrameWardMap').attr('src', mapurl);
		$viewContainer.find('.divWardNum').html('').hide();
	},
	LoadMap: function($viewContainer, address, isShowingCandidates) {	
	    // real call
		var mapServiceHost = "https://tigisst0";  //changed to "https" to force SSL
		if (window.location.protocol == "https:") { 
			mapServiceHost = "https://tigisst0.coc.ca";
		}
		//$.getJSON("https://digisst0/arcgis/rest/services/pub_GeospatialOperations/GetWardFromAddress/GPServer/Return%20Ward%20From%20Address/execute?Address=" + address + "&env%3AoutSR=&env%3AprocessSR=&returnZ=false&returnM=false&f=pjson", function (data) { // dev
	    //$.getJSON(mapServiceHost + "/arcgis/rest/services/pub_GeospatialOperations/GetWardFromAddress/GPServer/Return%20Ward%20From%20Address/execute?Address=" + address + "&env%3AoutSR=&env%3AprocessSR=&returnZ=false&returnM=false&f=pjson", function (data) { // test
		//$.getJSON("https://gis.calgary.ca/arcgis/rest/services/pub_IIS/GetWardFromAddress/GPServer/Return%20Ward%20From%20Address/execute?Address=" + address + "&env%3AoutSR=&env%3AprocessSR=&returnZ=false&returnM=false&f=pjson", function (data) { // prod - old wards (will become current wards as of sept 19 2017)
		$.getJSON("https://gis.calgary.ca/arcgis/rest/services/pub_IIS/NewWardFromAddress/GPServer/Return%20Ward%20From%20Address/execute?Address=" + address + "&env%3AoutSR=&env%3AprocessSR=&returnZ=false&returnM=false&f=pjson", function (data) { // prod - new wards (will be deprecated sept 19)
	        var items = [];
	
	        // in case we get no result
			mapurl = "https://thecityofcalgary.maps.arcgis.com/apps/Embed/index.html?webmap=d51ca635db74438dbcb7883640e6f292&amp;extent=-114.4815,50.8443,-113.595,51.1942&amp;home=true&amp;zoom=true&amp;scale=true&amp;search=true&amp;searchextent=true&amp;legendlayers=true&amp;basemap_gallery=true&amp;disable_scroll=false&amp;theme=light";
	
	        var ward = 0;
	        var linkurl = "";
	        var votingStation = 0;
	        var wardcopy = "";
	
	        $.each(data, function (key, val) {	
	            // we need to find the key on the server for the Ward and the LinkURL	
	        	if (key == 'results') {	
	                ward = val[0].value;	
	                linkurl = val[1].value;
	                votingStation = val[2].value;
	
	                if (linkurl.indexOf("?") >= 0) {
	                    mapurl = "https://maps.calgary.ca/CalgaryBoundaries/" + linkurl;
	                    wardcopy = "Ward " + ward;
	                } else if (linkurl.indexOf('http') >= 0) {
	                    mapurl = linkurl;
	                    wardcopy = "Ward " + ward;
	                } else if (linkurl.indexOf('not found') >= 0) {
	                    mapurl = "https://maps.calgary.ca/CalgaryBoundaries/";
	                    wardcopy = linkurl;	
	                } else {
	                    // what happened?
	                    console.error('COC: unknown state, message value = ' + linkurl);	
	                    // for testing
	                    mapurl = "https://maps.calgary.ca/CalgaryBoundaries" + linkurl;
	                    wardcopy = "Ward " + ward;
	                }	
	            } else {
	                // do nothing
	            }	
				mapurl = "https://thecityofcalgary.maps.arcgis.com/apps/Embed/index.html?webmap=d51ca635db74438dbcb7883640e6f292&amp;extent=-114.4815,50.8443,-113.595,51.1942&feature=Voting2017_PrivLayers_2272;Ward_NUM;" + ward;
	        });
	        $viewContainer.find('.iFrameWardMap').attr('src', mapurl);
	        if(wardcopy.indexOf("not found") >= 0) {
	        	var response = '<p><span style="font-weight:bold;">' + wardcopy + '</span></p>';
	        	response += '<p>Instructions:</p><ol style="margin-bottom: 0;"><li>Do not use suite or unit numbers.</li><li>Enter in your house number/building number and street name. E.g: 800 Macleod.</li><li>Wait for a list of addresses to be suggested.</li><li>Find your home address in that list and click on it</li><li>Click on "Check ward" button.</li></ol>';
		        $viewContainer.find('.divWardNum').show().html(response);
				COC.Analytics.wt({'name':'Check Ward Button','comment':'Invalid Address'});
		    } else {
		    	var response = 'You will be in <span style="font-weight:bold;">' + wardcopy + '</span>.';
		    	if(isShowingCandidates) {
		    		response += COC.Election.HtmlTemplates.FindMyWardLookupResult(ward);
		    	}
    	        var $wardNumberResultArea = $viewContainer.find('.divWardNum').show().html(response);
				// if elections web app (ie: elections.js loaded) then also initialize click events for the dynamically injected buttons:
				if(typeof Elect !== 'undefined' && Elect.ViewSectionsFromTxt && Elect.ViewNav) {
					Elect.ViewSectionsFromTxt.initViewBtnClickEvents($wardNumberResultArea);
					Elect.ViewNav.initHtmlFromTxtEvents($wardNumberResultArea);
				}
				if($viewContainer.selector === '#findVoteStation')
					COC.Election.VoteStn.setAndDisplayVoteStation(ward, votingStation);
				COC.Analytics.wt({'name':'Check Ward Button','comment':'Ward Found'});
    	    }
	    });
	},
};
COC.Election.VoteStationHtml = 
	'<a id="voteStation-mapUrl" class="ext-google-maps-link cui btn-md secondary-ghost" data-dest="Ranchlands+School" href="https://www.google.ca/maps/dir/Current+Location/Ranchlands+School/" target="_blank">' +
		'<div>Your voting station is <span id="voteStation-number" style="font-weight:bold;">201</span></div>' +
		'<div id="voteStation-name">Ranchlands School</div>' +
		'<div id="voteStation-address">610 Ranchlands BV NW</div>' +
        '<span class="ext-map-icon cicon-map2"></span>' +
	'</a>';
COC.Election.VoteStationButton = 
	'<a id="voteStation-mapUrl2" class="cui btn-md secondary-ghost" data-dest="Ranchlands+School" href="https://www.google.ca/maps/dir/Current+Location/Ranchlands+School/" target="_blank">Open in Google Maps</a>';
COC.Election.AdvanceVoteStationHtml = 
	'<div>' + 
		'<li>' +
			'<a class="voteStation-mapUrl" class="ext-google-maps-link cui btn-md secondary-ghost" href="https://www.google.ca/maps/dir/Current+Location/Ranchlands+School/" target="_blank">' +
				'<div class="voteStation-type">Vote bus</div>' +
				'<div class="voteStation-name">Ranchlands School</div>' +
				'<div class="voteStation-address">610 Ranchlands BV NW</div>' +
				'<div class="voteStation-days">October 4, 5, 6 - 9am to 5pm</div>' +
				'<div class="distance-container" style="display:none;">Distance: <span class="distance"></span></div>' +
		        '<span class="ext-map-icon cicon-map2"></span>' +
			'</a>' +
		'</li>' +
	'</div>';
COC.Election.VoteStations = {
	
	loadStnsJson: function() {
    	return new Promise(function(resolve, reject) {
	    	var jqxhr = $.getJSON('/election/Scripts/election/data/votingStations.json.txt', function() {
				console.log('success');
			})
			.done(function(data) {
				_stations = data;
				resolve('success');
			})
			.fail(function(error) {
				console.log('error');
			});
		});
    },    

};
COC.Election.AdVote = {   //Elections > Advanced Vote
	MILES_TO_KM: 1.2,  //Conversion constant for Miles to Kilometers that was more accurate than 1.609.
	isGeolocated: false,
	_advanceStations: [],

	init: function() {
		COC.Election.AdVote.initEvents();
		COC.Election.AdVote.showGeolocateButtonIfApp();
		COC.Election.AdVote.getAndShowAdvancedVoteStations();
	},
    initEvents: function() {
        $('#btnGeolocateMe').on('click', function(e) {
            COC.Election.AdVote.geolocateMe();
            e.preventDefault();
        });

    	$('#advanceVote_day').on('change', function(e) {
			COC.Election.AdVote.filterStations();
    	});
    	$('#advanceVote_quadrant').on('change', function(e) {
			COC.Election.AdVote.filterStations();
    	});
    	$('#advanceVote_locationType').on('change', function(e) {
			COC.Election.AdVote.filterStations();
    	});
    },
    showGeolocateButtonIfApp : function() {
    	// because Html5 geolocation feature is not available over http or over https while mixed content has not been allowed
    	var isApp = (!window.location.href.search('/election/Pages/'));
    	if(isApp) {
    		$('#divGeolocate').show();
    	}
    },
    getAndShowAdvancedVoteStations: function() {
    	var promise = COC.Election.VoteStn.loadStnsJson();
    	promise.then(function() {
    		COC.Election.AdVote.getStnObjects();
    		COC.Election.AdVote._advanceStations.sort(COC.Election.AdVote.sortStnObjectsByStationName);
    		COC.Election.AdVote._advanceStations.sort(COC.Election.AdVote.sortStnObjectsByQuadrant);
    		COC.Election.AdVote.displayStns();
    	});
    },
	sortStnObjectsByStationName: function(a, b){
		var aName = a.StationName;
		var bName = b.StationName;
		return ((aName > bName) ? -1 : ((aName < bName) ? 1 : 0));
	},
	sortStnObjectsByQuadrant: function(a, b){
		var aQuad = a.Quadrant;
		var bQuad = b.Quadrant;
		return ((aQuad > bQuad) ? -1 : ((aQuad < bQuad) ? 1 : 0));
	},
    getStnObjects: function() {
    	var stations = COC.Election.VoteStn._stations;
    	var advanceStations = [];
    	var i;
    	for(i = 0; i < stations.length; i++)
    		if(stations[i].WardNum === 0)
		    	advanceStations.push(stations[i]);
		COC.Election.AdVote._advanceStations = advanceStations;
    },
    displayStns: function() {
    	var advanceStationsHtml = '';
    	var advanceStations = COC.Election.AdVote._advanceStations;
    	var i;
    	for(i = 0; i < advanceStations.length; i++) {
    		var station = advanceStations[i];
    		var $html = $(COC.Election.AdvanceVoteStationHtml);
			if(station.Destination === "" || station.LatLon === "" || station.Type === 'Hospital')
				continue;  //do not process items that do not have a location or 'Hospital' locations
			switch(station.VsNum) {
				case 7:  // Municipal Building Atrium (City Hall) is closed for weekend
					$html.find('.voteStation-days').html('October 4, 5, 6, 10, 11 - Noon to 7pm').attr('data-days','4,5,6,10,11');
					break;
					
				case 63:  // was: 69
					$html.find('.voteStation-type').html('<span class="cicon-car"></span> Drive-up').attr('data-type','drive-up');
					$html.find('.voteStation-days').html('October 4, 5, 6 - 6:30am to 6:30pm').attr('data-days','4,5,6');
					break;
					
				case 66:
				case 67:
				case 68:
				case 69:
					$html.find('.voteStation-type').html('<span class="cicon-book"></span> Library').attr('data-type','library');
					$html.find('.voteStation-days').html('October 4, 5, 6, 10, 11 - 10am to 6pm<br />October 7, 8 (weekend) - Noon to 5pm').attr('data-days','4,5,6,7,8,10,11');
					break;
					
				case 82:
					$html.find('.voteStation-type').html('<span class="cicon-school"></span> Post-secondary').attr('data-type','post-secondary');
					$html.find('.voteStation-days').html('October 4, 5 - 10am to 6pm').attr('data-days','4,5');
					break;
				case 83:
					$html.find('.voteStation-type').html('<span class="cicon-school"></span> Post-secondary').attr('data-type','post-secondary');
					$html.find('.voteStation-days').html('October 4, 5, 6 - 10am to 6pm').attr('data-days','4,5,6');
					break;
				case 84:
					$html.find('.voteStation-type').html('<span class="cicon-school"></span> Post-secondary').attr('data-type','post-secondary');
					$html.find('.voteStation-days').html('October 4, 5, 6 - 10am to 6pm').attr('data-days','4,5,6');
					break;
				case 85:
					$html.find('.voteStation-type').html('<span class="cicon-school"></span> Post-secondary').attr('data-type','post-secondary');
					$html.find('.voteStation-days').html('October 4, 5, 6, 10, 11 - 10am to 6pm').attr('data-days','4,5,6,10,11');
					break;
				case 86:
					$html.find('.voteStation-type').html('Regular').attr('data-type','regular').hide();
					$html.find('.voteStation-days').html('October 4, 5, 6, 10, 11 - Noon to 5pm').attr('data-days','4,5,6,10,11');
					break;
				
				case 801: 
					$html.find('.voteStation-type').html('<span class="cicon-bus"></span> Vote bus').attr('data-type','vote-bus');
					$html.find('.voteStation-days').html('October 4 - 6:30am to 9:30am').attr('data-days','4');
					break;
				case 802:
					$html.find('.voteStation-type').html('<span class="cicon-bus"></span> Vote bus').attr('data-type','vote-bus');
					$html.find('.voteStation-days').html('October 4 - 11:30am to 2:00pm<br />October 11 - 11:30am to 2:00pm').attr('data-days','4,11');
					break;
				case 803:
					$html.find('.voteStation-type').html('<span class="cicon-bus"></span> Vote bus').attr('data-type','vote-bus');
					$html.find('.voteStation-days').html('October 4 - 3:30pm to 6:30pm').attr('data-days','4');
					break;
				case 804:
					$html.find('.voteStation-type').html('<span class="cicon-bus"></span> Vote bus').attr('data-type','vote-bus');
					$html.find('.voteStation-days').html('October 5 - 6:30am to 9:30am').attr('data-days','5');
					break;
				case 805:
					$html.find('.voteStation-type').html('<span class="cicon-bus"></span> Vote bus').attr('data-type','vote-bus');
					$html.find('.voteStation-days').html('October 5 - 11:30am to 2:00pm<br />October 10 - 11:30am to 2:00pm').attr('data-days','5,10');
					break;
				case 806:
					$html.find('.voteStation-type').html('<span class="cicon-bus"></span> Vote bus').attr('data-type','vote-bus');
					$html.find('.voteStation-days').html('October 5 - 3:30pm to 6:30pm').attr('data-days','5');
					break;
				case 807:					
					$html.find('.voteStation-type').html('<span class="cicon-bus"></span> Vote bus').attr('data-type','vote-bus');
					$html.find('.voteStation-days').html('October 6 - 6:30am to 9:30am').attr('data-days','6');
					break;
				case 808:
					$html.find('.voteStation-type').html('<span class="cicon-bus"></span> Vote bus').attr('data-type','vote-bus');
					$html.find('.voteStation-days').html('October 6 - 11:30am to 2:00pm').attr('data-days','6');
					break;
				case 809:
					$html.find('.voteStation-type').html('<span class="cicon-bus"></span> Vote bus').attr('data-type','vote-bus');
					$html.find('.voteStation-days').html('October 6 - 3:30pm to 6:30pm').attr('data-days','6');
					break;
				case 810:
					$html.find('.voteStation-type').html('<span class="cicon-bus"></span> Vote bus').attr('data-type','vote-bus');
					$html.find('.voteStation-days').html('October 10 - 6:30am to 9:30am').attr('data-days','10');
					break;
				case 811:
					$html.find('.voteStation-type').html('<span class="cicon-bus"></span> Vote bus').attr('data-type','vote-bus');
					$html.find('.voteStation-days').html('October 10 - 11:30am to 2:00pm').attr('data-days','10');
					break;
				case 812:
					$html.find('.voteStation-type').html('<span class="cicon-bus"></span> Vote bus').attr('data-type','vote-bus');
					$html.find('.voteStation-days').html('October 10 - 3:30pm to 6:30pm').attr('data-days','10');
					break;
				case 813:
					$html.find('.voteStation-type').html('<span class="cicon-bus"></span> Vote bus').attr('data-type','vote-bus');
					$html.find('.voteStation-days').html('October 11 - 6:30am to 9:30am').attr('data-days','11');
					break;
				case 814:
					$html.find('.voteStation-type').html('<span class="cicon-bus"></span> Vote bus').attr('data-type','vote-bus');
					$html.find('.voteStation-days').html('October 11 - 11:30am to 2:00pm').attr('data-days','11');
					break;
				case 815:
					$html.find('.voteStation-type').html('<span class="cicon-bus"></span> Vote bus').attr('data-type','vote-bus');
					$html.find('.voteStation-days').html('October 11 - 3:30pm to 6:30pm').attr('data-days','11');
					break;
				
				default:
					//Show the regular Advance vote day and times
					$html.find('.voteStation-type').html('Regular').attr('data-type','regular').hide();
					$html.find('.voteStation-days').html('October 4, 5, 6, 10, 11 - Noon to 7pm<br />October 7, 8 (weekend) - Noon to 5pm').attr('data-days','4,5,6,7,8,10,11');
					break;
			}
    		$html.find('li:first').attr('data-latlon', station.LatLon);
    		$html.find('a:first').attr('data-dest', station.Destination);
    		var url = "https://www.google.ca/maps/dir//" + station.Destination + "/";  //E.g. https://www.google.ca/maps/dir/Current+Location/Ranchlands+School/
			$html.find('.voteStation-mapUrl').attr('href', url);
			$html.find('.voteStation-mapUrl2').attr('href', url);
			$html.find('.voteStation-name').html(station.StationName);
			var addressHtml = (station.HouseNumber !== null && station.StreetName !== "" + station.StreetType !== "" && station.Quadrant !== "") ? (station.HouseNumber + ' ' + station.StreetName + ' ' + station.StreetType + ' ' + station.Quadrant) : station.Quadrant;
			$html.find('.voteStation-address').html(addressHtml).attr('data-quadrant',station.Quadrant);
			advanceStationsHtml += $html.html();
    	}
    	$('#locations').html(advanceStationsHtml);
    },
    geolocateMe: function() {
        $('#geolocateStatus').removeClass().addClass('cicon-spinner');
        try {
            navigator.geolocation.getCurrentPosition(COC.Election.AdVote.sortResultsByLocation, COC.Election.AdVote.geolocateError, {timeout:5000});
        } catch(ex) {
            $('#geolocateMsg').html('There was a problem locating you.');
        }
    },
    geolocateError: function(ex) {
        $('#geolocateMsg').html('There was a problem locating you.');
        console.warn('CoC (Exception Code: '+ex.code+'): '+ex.message);
    },
    sortResultsByLocation: function(position) {
        $('#geolocateStatus').removeClass().addClass('cicon-check');

        // Grab current position
        var latlon = new LatLon(position.coords.latitude, position.coords.longitude);

        // Show distance to each location and update link to google map
        $('#locations li').each(function() {
            // Update distance to location
            var loc = $(this).attr('data-latlon').split(',');
            //var distInKm = Math.round( latlon.distanceTo(new LatLon(Number(loc[0]),Number(loc[1]))) * COC.Election.AdVote.MILES_TO_KM);
            var distInKm = Math.round( latlon.distanceTo(new LatLon(Number(loc[0]),Number(loc[1]))) );
            $(this).find('.distance-container').show();
            $(this).find('.distance').html(distInKm + 'km');

            // Update link to google map that includes your location as starting point
            var a = $(this).find('a:last');
            a.attr('href', 'https://www.google.ca/maps/dir/'+ position.coords.latitude +','+ position.coords.longitude +'/'+ a.attr('data-dest') + '/');
        });

        // Sort locations by distance
        var locations = document.getElementById('locations');
        if(!locations)
        	return;
        var locationList = locations.querySelectorAll('li');
        var locationArray = Array.prototype.slice.call(locationList, 0);

        locationArray.sort(function(a,b){
            var locA  = a.getAttribute('data-latlon').split(',');
            var locB  = b.getAttribute('data-latlon').split(',');

            distA = latlon.distanceTo(new LatLon(Number(locA[0]),Number(locA[1])));
            distB = latlon.distanceTo(new LatLon(Number(locB[0]),Number(locB[1])));
            return distA - distB;
        });

        //Reorder the list
        locations.innerHTML = "";
        locationArray.forEach(function(el) {
            locations.appendChild(el);
        });
        
        COC.Election.AdVote.isGeolocated = true;
    },
    filterStations: function() {
   		var dayVal = $('#advanceVote_day').val();
   		var quadrantVal = $('#advanceVote_quadrant').val();
   		var locationTypeVal = $('#advanceVote_locationType').val();
		$('#locations li').each(function() {
			var dayMatch = (dayVal && dayVal !== 'any') ? false : true;
			if(dayVal) {
				var arr = ($(this).find('.voteStation-days').attr('data-days') || '').split(',');
				var i;
				for(i = 0; i < arr.length; i++)
					if(dayVal === arr[i])
						dayMatch = true;
			}
			var quadrantMatch = (quadrantVal && quadrantVal !== 'any') ? false : true;
			if(quadrantVal) {
				var val = ($(this).find('.voteStation-address').attr('data-quadrant') || '');
				if(quadrantVal === val)
					quadrantMatch = true;
			}
			var locationTypeMatch = (locationTypeVal && locationTypeVal !== 'any') ? false : true;
			if(locationTypeVal) {
				var val = ($(this).find('.voteStation-type').attr('data-type') || '');
				if(locationTypeVal === val)
					locationTypeMatch = true;
			}
			if(dayMatch && quadrantMatch && locationTypeMatch)
				$(this).show();
			else
				$(this).hide();
		});
    },
};
COC.Election.VoteStn = {  //Elections > Find my Vote Station
	_wardNum: undefined,
	_vsNum: undefined,
    _stationNum: undefined,
    _stations: [],
    _station: undefined,
    
    setAndDisplayVoteStation: function(wardNum, votingStationNum) {
    	COC.Election.VoteStn.reset();
    	COC.Election.VoteStn.setStnNumber(wardNum, votingStationNum);
    	COC.Election.VoteStn.getVoteStationDetailsByNumThenShow();
	},
	reset: function() {
	    COC.Election.VoteStn.clearVals();
    	COC.Election.VoteStn.hideDisplay();
    },
    clearVals: function() {
    	COC.Election.VoteStn._stationNum = undefined;
    	COC.Election.VoteStn._stations = [];
		COC.Election.VoteStn._station = undefined;
    },
    hideDisplay: function() {
    	$('#voteStation-container').hide();
    },
    setStnNumber: function(wardNum, votingStationNum) {
    	COC.Election.VoteStn._wardNum = wardNum;
    	COC.Election.VoteStn._vsNum = votingStationNum;    	
    	// E.g. if wardNum is 1 and votingStationNum is 1 then the set station num will be "101"
    	var strWardAndVotingStation = wardNum + COC.Election.VoteStn.convertNumberToTwoWholeDigitsInString(votingStationNum);
    	COC.Election.VoteStn._stationNum = strWardAndVotingStation;
    },
    // E.g. input 1 (number) and get back "01" (string) or input 11 and get back "11"
    convertNumberToTwoWholeDigitsInString: function(n) {
		return n > 9 ? "" + n: "0" + n;
	},
    getVoteStationDetailsByNumThenShow: function() {
    	var promise = COC.Election.VoteStn.loadStnsJson();
    	promise.then(function() {
    		COC.Election.VoteStn.getStnObject();
    		COC.Election.VoteStn.displayStn();
    	});
    },
    loadStnsJson: function() {
    	return new Promise(function(resolve, reject) {
    		if(COC.Election.VoteStn._stations.length > 0)
    			resolve('success');
    		
	    	var jqxhr = $.getJSON('/election/Scripts/election/data/votingStations.json.txt?v2', function() {
				console.log('success');
			})
			.done(function(data) {
				COC.Election.VoteStn._stations = data;
				resolve('success');
			})
			.fail(function(error) {
				console.log('error');
			});
		});
    },
    //votingStationNumber		integer		e.g. 201 means Ward 2, Voting Station #1
    getStnObject: function() {
    	var stationNum = COC.Election.VoteStn._stationNum;
    	var wardNum = parseInt(COC.Election.VoteStn._wardNum);  // was: parseInt(stationNum.toString().substr(0,1));
    	var vsNum = parseInt(COC.Election.VoteStn._vsNum);  // was: parseInt(stationNum.toString().substr(1,2));
    	var station;
    	var i;
    	for(i = 0; i < COC.Election.VoteStn._stations.length; i++) {
    		station = COC.Election.VoteStn._stations[i];
    		if(station.WardNum === wardNum && station.VsNum === vsNum)
    			COC.Election.VoteStn._station = station;
    	}
    },
    displayStn: function() {
    	var stationNum = COC.Election.VoteStn._stationNum;
    	var station = COC.Election.VoteStn._station;
		var url = "https://www.google.ca/maps/dir//" + station.Destination + "/";  //E.g. https://www.google.ca/maps/dir/Current+Location/Ranchlands+School/
		$('#voteStation-mapUrl').attr('href', url);
		$('#voteStation-mapUrl2').attr('href', url);
    	
    	$('#voteStation-number').text(stationNum);
    	$('#voteStation-name').html(station.StationName);
		var addressHtml = (station.HouseNumber !== null && station.StreetName !== "" + station.StreetType !== "" && station.Quadrant !== "") ? (station.HouseNumber + ' ' + station.StreetName + ' ' + station.StreetType + ' ' + station.Quadrant) : station.Quadrant;
    	$('#voteStation-address').html(addressHtml);
    	$('#voteStation-container').show();
    },
};
COC.Election.Profile = {
	Init: function(profileId) {
		COC.Election.Profile.getProfileListItemData(profileId);
	},
	getProfileListItemData: function(profileId) {
		if(!profileId)
			return;

		var getListConfig = {
			'subsite': '/election'
		};
		var profileListItemSoapConfig = {
	        'listName': 'CandidateProfiles', 
	        'query': '<Where><Or><Eq><FieldRef Name="ID" /><Value Type="Number">'+ profileId +'</Value></Eq><Eq><FieldRef Name="ProfileId" /><Value Type="Text">'+ profileId +'</Value></Eq></Or></Where>'
	    };
	    
		var profileListItem = 
		(COC.SPList.getListData( profileListItemSoapConfig, getListConfig ))
		.then(function(response) {
			if(response.length === 0) {
				COC.Election.Profile.showNoProfileFoundMessage();
				console.error('CoC: Valid but not found profile Id number passed in with profileId querystring parameter');
				return;
			}
			var item = response[0];
			COC.Election.Profile.parseProfileListItem(item);
			COC.Election.Profile.finalizeAndShowProfile();
		})
		.catch(function(response) {
			console.error(response);
		});
	},	
	showNoProfileFoundMessage: function() {
		$('.ele-candidate-profile-loading').hide();
		$('.ele-candidate-profile-not-found').show();
	},
	parseProfileListItem: function(item) {
		//Key:
		var id = COC.SPList.parseNumberFromItem(item, 'ID');
		var profileId = COC.SPList.parseStringFromItem(item, 'ProfileId');  // secondary key
		
		//Required fields:
		var candidateType = COC.SPList.parseStringFromItem(item, 'CandidateType');
		var fullName = COC.SPList.parseStringFromItem(item, 'Title');
		
		//Optional but required in some cases:
		var wardNum = COC.SPList.parseNumberFromItem(item, 'Ward');
		if(candidateType === "Councillor" && !wardNum) {
			COC.Election.Profile.showNoProfileFoundMessage();
			$('.ele-candidate-profile-not-found').html('<p>Must specify a Ward number for Candidate Type: Councillor</p>');
			console.error('CoC: Must specify a Ward number for Candidate Type: Councillor');
			return;
		}
		var strWardDescription = 'Ward '+wardNum;
		var wardGroup = COC.SPList.parseStringFromItem(item, 'WardGroup');
		var wardNum2;		if(candidateType === "Public school trustee")
			candidateType = "Public School Trustee";
		if(candidateType === "Separate school trustee")
			candidateType = "Separate School Trustee";
		if(candidateType === "Mayoral") {
			// select "Mayoral" local nav menu section
			$('.candidate-profile-local-nav > li.nav-item:eq(0)').addClass('active');
			$('#localNavContainer .localNav .nav-menu li.nav-item:eq(0)').addClass('active');
		}
		if(candidateType === "Councillor") {
			$('.candidate-profile-local-nav').ready(function() {
				COC.Election.Profile.deselectLocalNavMenuItems();
				// select "Councillor" local nav menu section item
				var $navMenuGroup = $('.candidate-profile-local-nav ul.sub-nav:eq(0)');
				$navMenuGroup.find('li').show();
				$navMenuGroup.find('li[data-ward="'+ wardNum +'"]').addClass('active');
				var $navMenuGroup2 = $('#localNavContainer .localNav .nav-menu ul.sub-nav:eq(0)');
				$navMenuGroup2.find('li').show();
				$navMenuGroup2.find('li[data-ward="'+ wardNum +'"]').addClass('active');				
			});
		}
		if((candidateType === "Public School Trustee" || candidateType === "Separate School Trustee")) {
			if(!wardGroup) {
				COC.Election.Profile.showNoProfileFoundMessage();
				$('.ele-candidate-profile-not-found').html('<p>Must specify a WardGroup for Candidate Type: '+ candidateType +'</p>');
				console.error('CoC: Must specify a WardGroup for Candidate Type: '+ candidateType);
				return;
			}
			strWardDescription = 'Wards '+wardGroup;
			var navMenuSel = wardNum;
			if(wardGroup.toUpperCase() !== 'OTHER') {
				var tmpWardNums = wardGroup.split('&');  // E.g. ["1 "," 2 (plus Cochrane)"]
				wardNum = parseInt(tmpWardNums[0].trim());  // E.g 1
				wardNum2 = parseInt(tmpWardNums[1].trim().split(' ')[0]);  // E.g. 2
				navMenuSel = wardNum2;
				if(tmpWardNums.length === 2) {
					if(wardGroup.search('Airdrie'))
						navMenuSel = 'airdrie';
					if(wardGroup.search('Cochrane'))
						navMenuSel = 'cochrane';
					if(wardGroup.search('Chestermere'))
						navMenuSel = 'chestermere';
				}
			} else {
				wardNum = undefined;
				navMenuSel = 'other';
			}
			$('.candidate-profile-local-nav').ready(function() {
				COC.Election.Profile.deselectLocalNavMenuItems();
				// select "School Trustee" local nav menu section item
				var $navMenuGroup = $('.candidate-profile-local-nav ul.sub-nav:eq(1)');
				$navMenuGroup.find('li').show();
				$navMenuGroup.find('li[data-ward="'+ wardNum +'"],li[data-ward="'+ wardNum2 +'"],li[data-ward="'+ navMenuSel +'"]').addClass('active');
				var $navMenuGroup2 = $('#localNavContainer .localNav .nav-menu ul.sub-nav:eq(1)');
				$navMenuGroup2.find('li').show();
				$navMenuGroup2.find('li[data-ward="'+ wardNum +'"],li[data-ward="'+ wardNum2 +'"],li[data-ward="'+ navMenuSel +'"]').addClass('active');
			});
		}
		
		//Optional fields:
		var photo = COC.SPList.parseUrlFromItem(item, 'Photo') || { url:'/election/Scripts/election/images/candidates/person.svg', description:'No profile image supplied'};
		//var youtubeDescription = COC.SPList.parseStringFromItem(item, 'Description');
		var platform = COC.SPList.parseMultilineStringFromItem(item, 'ElectionPlatform');  //multi-line
		var youtubeVideoId = COC.SPList.parseStringFromItem(item, 'YoutubeVideoID');
		var biography = COC.SPList.parseMultilineStringFromItem(item, 'Biography');  //multi-line
		var phone = COC.SPList.parseStringFromItem(item, 'Phone');
		var email = COC.SPList.parseStringFromItem(item, 'Email');
		var website = COC.SPList.parseUrlFromItem(item, 'Website');
		var facebookUrl = COC.SPList.parseUrlFromItem(item, 'FacebookUrl');
		var twitterUrl = COC.SPList.parseUrlFromItem(item, 'TwitterUrl');
		var youtubeUrl = COC.SPList.parseUrlFromItem(item, 'YoutubeUrl');
		var linkedInUrl = COC.SPList.parseUrlFromItem(item, 'LinkedinUrl');
		var instagramUrl = COC.SPList.parseUrlFromItem(item, 'InstagramUrl');
		var flickrUrl = COC.SPList.parseUrlFromItem(item, 'FlickrUrl');
		var vimeoUrl = COC.SPList.parseUrlFromItem(item, 'VimeoUrl');
		var pinterestUrl = COC.SPList.parseUrlFromItem(item, 'PinterestUrl');
		var snapchatUrl = COC.SPList.parseUrlFromItem(item, 'SnapchatUrl');
		var wechatUrl = COC.SPList.parseUrlFromItem(item, 'WechatUrl');
		
		//Generated fields:
		var wardAndCandidateType = (((wardNum || wardGroup) && candidateType !== 'Mayoral') ? strWardDescription+', ' : '') + candidateType+' Candidate';  //E.g. "Mayoral" or "Ward 2, Councillor" or "Ward 10 & 14 (plus Cochrane), Public School Trustee"
		var goBackUrl = '';
		var goBackBtnHtml = '';
		switch(candidateType) {
			case 'Mayoral':
				goBackBtnHtml = COC.Election.HtmlTemplates.ProfileBtnBackToMayoral();
				break;
			case 'Councillor':
				goBackBtnHtml = COC.Election.HtmlTemplates.ProfileBtnBackToCouncillorWard(wardNum);
				break;
			case 'Public School Trustee':
				goBackBtnHtml = COC.Election.HtmlTemplates.ProfileBtnBackToSchoolWard(wardNum, wardNum2);
				break;
			case 'Separate School Trustee':
				if(wardGroup.toUpperCase() === 'OTHER')
					goBackBtnHtml = COC.Election.HtmlTemplates.ProfileBtnBackToOtherSeparateSchool();
				else
					goBackBtnHtml = COC.Election.HtmlTemplates.ProfileBtnBackToSchoolWard(wardNum, wardNum2);
				break;
			default:
				break;
		}
		
		//Populate page
		$('#btnGoBackContainer').html(goBackBtnHtml).show();
		$('#elePhoto').attr('src', photo.url).attr('title', photo.description);
		$('#eleFullName').html(fullName);
		$('#eleWardAndType').html( wardAndCandidateType );
		
		// if no platform or bio info for candidate then show 'no profile info' message (a video, social media or contact info doesn't count)
		if(!platform && !biography) {
			$('#ele-no-profile-info').show();
		}
		
		if(platform) {
			var arrStartLen = 1;
			if(platform[0].length < 100 && platform.length > 1) {
				// first paragraph too short. Show first 2 paragraphs.
				arrStartLen = 2;
				$('#elePlatform').html( '<p>'+platform[0]+'</p><p>'+platform[1]+'</p>' );
			} else {
				// just show first paragraph
				$('#elePlatform').html( '<p>'+platform[0]+'</p>' );
			}			
			$('#ePlatform').show();
			if(platform.length > arrStartLen) {			
				//process remaining paragraphs from multiline text field to appear when you click to "read more"
				var i, platformMoreHtml = '';
		        for(i=arrStartLen; i<platform.length; i++) {  //begin iteration from 2nd array element
		            platformMoreHtml += '<p>'+platform[i]+'</p>';
		        }		        
				$('#elePlatformMore').html( platformMoreHtml );				
				//set up read more/less buttons
				$('.ele-platform-btn-read-more').show();
				$('.ele-platform-btn-read-more').click(function() {
					//expand more to read
					$('.ele-platform-btn-read-more').hide();
					$('.ele-platform-btn-read-less, .ele-platform-read-more').show();
				});
				$('.ele-platform-btn-read-less').click(function() {
					//collapse
					$('.ele-platform-btn-read-less, .ele-platform-read-more').hide();
					$('.ele-platform-btn-read-more').show();
				});
			} else {
				$('.ele-platform-btn-read-more').hide();
			}
		}

		if(youtubeVideoId) {
			if(youtubeVideoId === 'COMINGSOON')
				COC.Election.Profile.populateVideoSectionComingSoon();
			else
				COC.Election.Profile.populateVideoSection(youtubeVideoId, fullName, wardAndCandidateType);
		}
		
		if(biography) {
			var arrStartLen = 1;
			if(biography[0].length < 100 && biography.length > 1) {
				// first paragraph too short. Show first 2 paragraphs.
				arrStartLen = 2;
				$('#eleBiography').html( '<p>'+biography[0]+'</p><p>'+biography[1]+'</p>' );
			} else {
				// just show first paragraph
				$('#eleBiography').html( '<p>'+biography[0]+'</p>' );
			}
			$('#eBiography').show();
			if(biography.length > arrStartLen) {			
				//process remaining paragraphs from multiline text field to appear when you click to "read more"
				var i, biographyMoreHtml = '';
		        for(i=arrStartLen; i<biography.length; i++) {  //begin iteration from 2nd array element
		            biographyMoreHtml += '<p>'+biography[i]+'</p>';
		        }		        
				$('#eleBiographyMore').html( biographyMoreHtml );				
				//set up read more/less buttons
				$('.ele-bio-btn-read-more').show();
				$('.ele-bio-btn-read-more').click(function() {
					//expand more to read
					$('.ele-bio-btn-read-more').hide();
					$('.ele-bio-btn-read-less, .ele-bio-read-more').show();
				});
				$('.ele-bio-btn-read-less').click(function() {
					//collapse
					$('.ele-bio-btn-read-less, .ele-bio-read-more').hide();
					$('.ele-bio-btn-read-more').show();
				});
			} else {
				$('.ele-bio-btn-read-more').hide();
			}
		}
		
		if(facebookUrl || twitterUrl || youtubeUrl || linkedInUrl || instagramUrl || flickrUrl || vimeoUrl || pinterestUrl || snapchatUrl || wechatUrl) {
			var socialHtml = '';
			if(facebookUrl)
				socialHtml += '<div class="col-md-4"><span class="cicon-facebook"></span> <a href="'+ facebookUrl.url +'" target="_blank" title="Facebook: '+ facebookUrl.description +'">Facebook</a></div>';
			if(twitterUrl)
				socialHtml += '<div class="col-md-4"><span class="cicon-twitter"></span> <a href="'+ twitterUrl.url +'" target="_blank" title="Twitter: '+ twitterUrl.description +'">Twitter</a></div>';
			if(youtubeUrl)
				socialHtml += '<div class="col-md-4"><span class="cicon-youtube"></span> <a href="'+ youtubeUrl.url +'" target="_blank" title="Youtube: '+ youtubeUrl.description +'">Youtube</a></div>';
			if(linkedInUrl)
				socialHtml += '<div class="col-md-4"><span class="cicon-linkedin"></span> <a href="'+ linkedInUrl.url +'" target="_blank" title="LinkedIn: '+ linkedInUrl.description +'">LinkedIn</a></div>';
			if(instagramUrl)
				socialHtml += '<div class="col-md-4"><span class="cicon-instagram"></span> <a href="'+ instagramUrl.url +'" target="_blank" title="Instagram: '+ instagramUrl.description +'">Instagram</a></div>';
			if(flickrUrl)
				socialHtml += '<div class="col-md-4"><span class="cicon-flickr"></span> <a href="'+ flickrUrl.url +'" target="_blank" title="Flickr: '+ flickrUrl.description +'">Flickr</a></div>';
			if(vimeoUrl)
				socialHtml += '<div class="col-md-4"><span class="cicon-vimeo"></span> <a href="'+ vimeoUrl.url +'" target="_blank" title="Vimeo: '+ vimeoUrl.description +'">Vimeo</a></div>';
			if(pinterestUrl)
				socialHtml += '<div class="col-md-4"><span class="cicon-pinterest"></span> <a href="'+ pinterestUrl.url +'" target="_blank" title="Pinterest: '+ pinterestUrl.description +'">Pinterest</a></div>';
			if(snapchatUrl)
				socialHtml += '<div class="col-md-4"><span class="cicon-external-link"></span> <a href="'+ snapchatUrl.url +'" target="_blank" title="Snaphat: '+ snapchatUrl.description +'">Snaphat</a></div>';
			if(wechatUrl)
				socialHtml += '<div class="col-md-4"><span class="cicon-external-link"></span> <a href="'+ wechatUrl.url +'" target="_blank" title="WeChat: '+ wechatUrl.description +'">WeChat</a></div>';
			
			$('#ele-social-columns').html(socialHtml);
			$('#ele-social-container').show();
		}
		
		if(phone || email || website) {
			var contactHtml = '';
			if(phone)
				contactHtml += '<div class="col-md-4"><span class="cicon-phone"></span> <a href="tel:'+ phone +'" title="Phone">'+ phone +'</a></div>';
			if(email)
				contactHtml += '<div class="col-md-4"><span class="cicon-envelope-o"></span> <a href="mailto:'+ email +'" title="Email">'+ email +'</a></div>';
			if(website)
				contactHtml += '<div class="col-md-4"><span class="cicon-globe"></span> <a href="'+ website.url +'" target="_blank" title="Website">'+ website.description +'</a></div>';
			
			$('#ele-contact-columns').html(contactHtml);
			$('#ele-contact-info').show();
		}
		
	},
	deselectLocalNavMenuItems: function() {
		// de-select active local nav menu section or section item
		$('.candidate-profile-local-nav li').removeClass('active');
		$('.candidate-profile-local-nav ul.sub-nav').removeClass('active');
		$('.candidate-profile-local-nav ul.sub-nav li').hide();
		$('#localNavContainer .localNav .nav-menu li').removeClass('active');
		$('#localNavContainer .localNav .nav-menu ul.sub-nav').removeClass('active');
		$('#localNavContainer .localNav .nav-menu ul.sub-nav li').hide();

	},
	finalizeAndShowProfile: function() {
		var $profilePageContent = $('.ele-candidate-profile');
		
		// if elections web app (ie: elections.js loaded) then also initialize click events for the dynamically injected buttons:
		if(typeof Elect !== 'undefined' && Elect.ViewSectionsFromTxt && Elect.ViewNav) {
			Elect.ViewSectionsFromTxt.initViewBtnClickEvents($profilePageContent);
			Elect.ViewNav.initHtmlFromTxtEvents($profilePageContent);
		}
		//Show populated page
		$('.ele-candidate-profile-loading').hide();
		$profilePageContent.show();
	},
	populateVideoSection: function(youtubeVideoId, fullName, wardAndCandidateType) {
		var youtubeVideoTitle = 'Youtube: '+ fullName +', '+ wardAndCandidateType;
		//$('#eleVideoDescription').html( (youtubeDescription ? youtubeDescription: '') );
		$('.youtube-container').attr('data-embed-url', 'https://www.youtube.com/embed/'+youtubeVideoId+'?rel=0&amp;autoplay=1');
		$('#eleVideoLink').attr('href', 'https://www.youtube.com/watch?v='+youtubeVideoId+'&rel=0');  // was chained: .html( youtubeVideoTitle );
		$('#rtcPlaylistOverlay').html( youtubeVideoTitle );
		
		COC.Election.Profile.initYoutubeEvents();

		//show section
		$('#ele-video-container').show();
	},
	initYoutubeEvents: function() {
		// load the youtube embed (can be a single video or a playlist) when overlay graphic is clicked
		$('.youtube-container .youtubePlayOverlay').on('click', COC.Election.Profile.videoPlayFromOverlay);
	},
    videoPlayFromOverlay: function() {
        var $youtubeContainer = $(this).closest('.youtube-container');
    	var $youtubeIFrame = $youtubeContainer.find('iframe:first');
        $(this).hide();
        $youtubeIFrame.show().attr('src', $youtubeContainer.attr('data-embed-url'));
    },
    populateVideoSectionComingSoon: function() {
    	$('#ele-video-container .row:first').html('<div class="col-md-12"><p>Candidate video is coming soon</p></div>');
    	
		//show section
		$('#ele-video-container').show();
    },
};
COC.Election.News = {
	// Requirements for Pages to appear in the "Latest Election News" section are as follows:
	// Content type: 'Article Page'.
	// Content Classification: 'Election > News' - Note: Page should also exist in the '/election/Pages/news/' folder for better organization.
	// Article Date: field is required even though it's an optional Page property in SharePoint - Note: A date in the past or current day will make article appear. Publish an article with an article date in the future and it won't appear until it is after or equal to that day.
	// Page must be published and approved.
	InitNewsSummary: function(selNewsContainer) {
		COC.Election.News.InitNews(selNewsContainer, true);
	},
	InitNews: function(selNewsContainer, isSummary /*optional*/) {
		var isSummary = (typeof isSummary !== 'undefined') ? isSummary : false;
		var listName = "Pages";
		var soapEnv = COC.Election.News.GetNewsPagesSoapEnvelope;
		$.ajax({
			url: "/election/_vti_bin/lists.asmx",  //?r=" + coc_re.generateRandomNumber(),
			async: false,
			cache: false,
			type: "POST",
			contentType: "text/xml; charset='utf-8'",
			dataType: "xml",
			data: soapEnv,	
			error: function(xhr, status, error) {
				console.error(xhr.responseText);
			},
			success: function(data, status, xhr) {				
				var $xmlResponse = xhr.responseXML || xhr.responseText;
				var arrNews = COC.Election.News.ProcessNews($xmlResponse);
				var newsHtml = (isSummary) ? COC.Election.News.ProcessNewsSummaryHtml(arrNews) : COC.Election.News.ProcessNewsHtml(arrNews);
				$(selNewsContainer).append(newsHtml);
		   	}
		});
	},
	// News pages that will be processed are: Published Pages in /election/ sub-site that have Content Classification of "Election > News" and Article Date in the past or present day
	ProcessNews: function($xmlResponse) {
		var arrNews = [];
		$($xmlResponse).find("z\\:row, row").each(function() {
			var curRow = $(this);
			var contentClassification = curRow.attr('ows_Content_x0020_Classification') || '';
			var isNewsPage = ( contentClassification.search('News') > -1 ) ? true : false;
			//var isNewsPage = ( curRow.attr('ows_Content_x0020_Classification').search('News') > -1 ) ? true : false;
			if(isNewsPage) {
				var publishedState = curRow.attr('ows__Level') || '1';
				var isPublished = (parseInt(publishedState) <= 1) ? true : false;  // Level: "1" == published. Level: "2" or greater == checked out, etc.				
				var articleDate = curRow.attr('ows_ArticleStartDate') || '';  //e.g: 2017-05-16 16:44:42
				articleDate = articleDate.split(' ')[0];
				var articleDateTime = (new Date(articleDate)).getTime();
				
				//if(isPublished && COC.Election.News.CheckIfDateTimeInPast(articleDateTime)) {
				if(isPublished) {
					var articleDateFormatted = (new Date(articleDate)).format('mmm d, yyyy');					
					var title = curRow.attr('ows_Title') || '';
					var pageContentSummary = curRow.attr('ows_ArticleByLine') || '';					
					var pageUrl = '/' + curRow.attr('ows_FileRef').split('#')[1];					
					arrNews.push({
						'title': title,
						'pageContentSummary': pageContentSummary,
						'pageUrl': pageUrl,
						'articleDateTime': articleDateTime,
						'articleDateFormatted': articleDateFormatted,
					});
				}
			}
		});
		arrNews = arrNews.sort(COC.Election.News.SortByDateTimeDescending);
		return arrNews;
	},
	CheckIfDateTimeInPast: function(dateTime) {
		return (dateTime <= (new Date().getTime()));
	},
	ProcessNewsSummaryHtml: function(arrNews) {
		var cntNews = 0;
		var newsItemsHtml = '';
		var i;
		for(i = 0; i < arrNews.length; i++) {
			cntNews++;
			if(cntNews > 3)
				break;
			newsItemsHtml += 
			'<div class="col-md-4">' +
			'	<p>' +
			'		<datetime>'+ arrNews[i].articleDateFormatted +'</datetime>' +
			'	</p>' +
			'	<a href="'+ arrNews[i].pageUrl +'">' +
			'		<span>'+ arrNews[i].title +'</span>' +
			'	</a>' +
			'	<p>'+ arrNews[i].pageContentSummary + '</p>' +
			'</div>';
		}
		return newsItemsHtml;
	},
	ProcessNewsHtml: function(arrNews) {
		var newsItemsHtml = '';
		var i;
		for(i = 0; i < arrNews.length; i++) {
			newsItemsHtml += 
			'<div>' +
			'	<p>' +
			'		<datetime>'+ arrNews[i].articleDateFormatted +'</datetime>' +
			'	</p>' +
			'	<a href="'+ arrNews[i].pageUrl +'">' +
			'		<span>'+ arrNews[i].title +'</span>' +
			'	</a>' +
			'	<p>'+ arrNews[i].pageContentSummary + '</p>' +
			'</div>';
		}
		return newsItemsHtml;
	},
	SortByDateTimeDescending: function(a, b){
		var aTime = a.articleDateTime;
		var bTime = b.articleDateTime; 
		return ((aTime > bTime) ? -1 : ((aTime < bTime) ? 1 : 0));
	},
	GetNewsPagesSoapEnvelope:
        "<?xml version='1.0' encoding='utf-8'?>" +
        "<soapenv:Envelope " + 
            "xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' " +
            "xmlns:api='http://127.0.0.1/Integrics/Enswitch/API' " +
            "xmlns:xsd='http://www.w3.org/2001/XMLSchema' " +
            "xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'>" +
            "<soapenv:Body>" +
	      		"<GetListItems xmlns='http://schemas.microsoft.com/sharepoint/soap/'>" +
		    		"<listName>Pages</listName>" +
		    		"<query>" +
						"<Query>" +
							"<OrderBy>" +
								"<FieldRef Name='Title' Ascending='TRUE' />" +
							"</OrderBy>"+
						"</Query>" +
					"</query>" +
					"<viewFields>" +
						"<ViewFields>" +
							"<FieldRef Name='ID' />" +
							"<FieldRef Name='Title' />" +
							"<FieldRef Name='PublishingPageContent' />" +
							"<FieldRef Name='PublishingStartDate' />" +
							"<FieldRef Name='Content_x0020_Classification' />" +
							"<FieldRef Name='COCIS_x0020_Keywords' />" +
							"<FieldRef Name='ArticleStartDate' />" +
							"<FieldRef Name='ArticleByLine' />" +
						"</ViewFields>" +
					"</viewFields>" +
					"<rowLimit>5000</rowLimit>" + 
					"<queryOptions>" +
						"<QueryOptions>" +
							"<ViewAttributes Scope='Recursive' />" +
						"</QueryOptions>" +
					"</queryOptions>" +
	      		"</GetListItems>" +
            "</soapenv:Body>" +
        "</soapenv:Envelope>",

};
COC.Election.Countdown = {
	Init: function(selContainer, strTimeText) {
		var strTimeText = (typeof strTimeText !== 'undefined') ? strTimeText : "2017/10/16 10:00:00";
		$(selContainer).jCountdown({
			timeText:strTimeText,
			timeZone:-6,
			style:"slide",
			color:"white",
			width:230,         /* default 200  */
			textGroupSpace:2,
			textSpace:0,
			reflection:false,
			dayTextNumber:3,   /* updated for byelection Feb 2 2015 orig 4 */
			displayDay:true,
			displayHour:true,
			displayMinute:true,
			displaySecond:true,
			displayLabel:true,
			onFinish:function(){
				//alert("finish");
				// TODO: add in functionality to swap out the countdown with a graphic (including a call to action in graphic) upon completion, and graphic will link to a page on the site.
			}
		});
		
		var $countdown = $(selContainer);
		$countdown.find('.label:eq(0)').text('Days');
		$countdown.find('.label:eq(1)').text('Hours');
		$countdown.find('.label:eq(2)').text('Min');
		$countdown.find('.label:eq(3)').text('Sec');
	},
};
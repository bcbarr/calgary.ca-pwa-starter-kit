//Helper functions for retrieving and parsing data from SharePoint lists
var COC = COC || {};
COC.SPList = {

    //Enumerators - types of columns that this class can understand:
    STRING: 'String',
    NUMBER: 'Number',
    URL: 'Url',
    MULTILINESTRING: 'MultilineString',

    // Request SharePoint 2010 List data and get a Promise that will send the result. It constructs a SOAP request and helps parse the response in to Json, returning the parsed response in a Javascript Promise.
    // Other than passing in CAML 'query' or 'queryOptions' it keeps SOAP envelope writing to a minimum. Or, pass no parameters and simply call COC.SPList.getListData() to get shallow Pages list data from website root.
    //  soapConfig          object (optional)   soapConfig object gets passed along to function: createSoapEnvelope. See createSoapEnvelope for details about this object.
    //  getListConfig       object (optional)   parameters (all are optional):
    //    'jsonp'             boolean             false by default - false triggers request from Sharepoint web service. Set to true to request jsonp from external web service to get around CORS.
    //    'subsite'           string              subsite path, without trailing '/' for where to get list data from - e.g. '/CFOD' or '/CFOD/CSC'
    //    'noParse'           boolean             true by default - false triggers parsing of Xml row nodes that are returned. Set to true to request row nodes then you may choose to parse data manually with COC.SPList parser methods.
    //    'cols'              array of objects: { name:[string], type:[string] }   empty object by default - combined with noParse:false the cols supplies this method with column names and each type so that it can all be parsed for you; Specify what columns you're expecting back and the data type - E.g. [{name:'Title'},{name:'ID',type:COC.SPList.NUMBER},{name:'Website',type:COC.SPList.URL}]
    //  @returns            Promise(array)      contains 'z:row' node elements of an XMLDocument javascript object
    getListData : function(soapConfig, getListConfig) {
        var getListConfig = (typeof getListConfig !== 'undefined') ? getListConfig : {'jsonp':false,'subsite':''};
        getListConfig.subsite = (typeof getListConfig.subsite !== 'undefined') ? getListConfig.subsite : '';
        getListConfig.jsonp = (typeof getListConfig.jsonp !== 'undefined') ? getListConfig.jsonp : false;
        getListConfig.noParse = (typeof getListConfig.noParse !== 'undefined') ? getListConfig.noParse : true;
        getListConfig.cols = (typeof getListConfig.cols !== 'undefined') ? getListConfig.cols : {};
        var soapConfig = (typeof soapConfig !== 'undefined') ? soapConfig : {'listName':'Pages'};

        var soapEnv = COC.SPList.createSoapEnvelope(soapConfig);
        
        return new Promise(function(resolve, reject) {
            var path = getListConfig.subsite + '/_vti_bin/lists.asmx';  //E.g. '/cfod/csc/_vti_bin/lists.asmx'  or at the root: '/_vti_bin/lists.asmx'
            if(getListConfig.jsonp) {
                // request as Jsonp through custom web service to get around CORS
                path = "http://cloud.cocnmp.com/GetSPListSoap.ashx?soapEnvelope=" + encodeURIComponent(soapEnv); //E.g. "http://cloud.cocnmp.com/GetSPListSoap.ashx?soapEnvelope=%3Csoapenv%3AEnvelope%20xmlns%3Asoapenv%3D'http%3A%2F%2Fschemas.xmlsoap.org%2Fsoap%2Fenvelope%2F'%3E%3Csoapenv%3ABody%3E%3CGetListItems%20xmlns%3D'http%3A%2F%2Fschemas.microsoft.com%2Fsharepoint%2Fsoap%2F'%3E%3ClistName%3EAlertBanner%3C%2FlistName%3E%3CviewFields%3E%3CViewFields%3E%3CFieldRef%20Name%3D'ows_Title'%20%2F%3E%3CFieldRef%20Name%3D'AlertMessage'%20%2F%3E%3CFieldRef%20Name%3D'AlertMoreInfoLink'%20%2F%3E%3CFieldRef%20Name%3D'ows_AlertType'%20%2F%3E%3CFieldRef%20Name%3D'AlertHtml'%20%2F%3E%3CFieldRef%20Name%3D'CocisIsPublic'%20%2F%3E%3CFieldRef%20Name%3D'Is_x0020_Public'%20%2F%3E%3CFieldRef%20Name%3D'Page_x0020_Path'%20%2F%3E%3C%2FViewFields%3E%3C%2FviewFields%3E%3CrowLimit%3E200%3C%2FrowLimit%3E%3CqueryOptions%3E%3CQueryOptions%3E%3CViewAttributes%20Scope%3D'Recursive'%20%2F%3E%3C%2FQueryOptions%3E%3C%2FqueryOptions%3E%3C%2FGetListItems%3E%3C%2Fsoapenv%3ABody%3E%3C%2Fsoapenv%3AEnvelope%3E"
            }
            //set up xhr
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function() {
                //this is called even on 404 etc and multiple times before fully processed so, check the status
                var ready = (xhr.readyState == 4 && xhr.status == 200);
                if(!ready) return;
                //process response
                var xmlDoc;
                if(xhr.responseText) {
                    xmlDoc = COC.SPList.parseXmlDocFromString(xhr.responseText);
                } else {
                    if(!xhr.responseXML)
                        reject(Error('No XML response'));
                    xmlDoc = xhr.responseXML;
                }
                //xml rows may have either node name: 'z:row' or 'row'
                var xmlRows = COC.SPList.getListRowsFromXMLDocument(xmlDoc);
                xmlRows = (xmlRows.length > 0) ? xmlRows : COC.SPList.getListRowsFromXMLDocument(xmlDoc);

                if(getListConfig.noParse) {
                    resolve(xmlRows);
                    return;
                }
                var rows = COC.SPList.parseListDataFromRowXMLNodes(getListConfig.columns, xmlRows);
                resolve(rows);
            };
            //handle errors
            var errorResponse = function() {  reject(Error(xhr.statusText));  };
            xhr.ontimeout = errorResponse;
            xhr.onabort = errorResponse;
            xhr.onerror = errorResponse;
            //send request
            if(getListConfig.jsonp) {
                xhr.open('GET', path, true);
            } else {
                xhr.open('POST', path, true);
                xhr.setRequestHeader('Content-Type', 'text/xml; charset="utf-8"');
                xhr.setRequestHeader('Accept', 'application/xml, text/xml');
            }
            xhr.send(soapEnv);
        });
    },

    // Helper function to create XMLDocument object from string with fallback for old IE
    parseXmlDocFromString : function(xmlStr) {
        if (window.DOMParser) {
            return ( new window.DOMParser() ).parseFromString(xmlStr, "text/xml");
        } else if (typeof window.ActiveXObject != "undefined" && new window.ActiveXObject("Microsoft.XMLDOM")) {
            // Fallback for old IE
            var xmlDoc = new window.ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async = "false";
            xmlDoc.loadXML(xmlStr);
            return xmlDoc;
        } else {
            return null;
        }
    },

    getListRowsFromXMLDocument: function(xmlDoc) {
        // Traverse XMLDoc to get row or just use commented out jquery version
        try {
            //xml rows may have either node name: 'z:row' or 'row'
            var xmlRows = xmlDoc.getElementsByTagName('z:row');
            xmlRows = (xmlRows.length > 0) ? xmlRows : xmlDoc.getElementsByTagName('row');
            return xmlRows;
            //NOTE: Traversing directly to row nodes may be faster that the above:
            //return xmlDoc.children.item('soap:envelope').children.item('soap:body').children.item('getlistitemresponse').children.item('getlistitemresult').children.item('listitems').children.item('rs:data').children.item('z\\:row, row');
            //alternate jquery version to traverse XMLDocument: //return $(xmlDoc).find('z\\:row, row').first()[0];
        } catch(err) {
            console.error('CoC Error: Could not get z:row data by traversing XMLDocument object');
            return undefined;
        }
    },

    parseListDataFromRowXMLNodes: function(arrColumns, rowXmlNodes) {
        var i, arrParsedRowData = new Array(rowXmlNodes.length);
        for(var row in rowXmlNodes) {
            if(!rowXmlNodes.hasOwnProperty(row)) continue;
            var objRow = {};
            for(i = 0; i < arrColumns.length; i++) {
                if(row.hasAttribute('ows_'+arrColumns[i].name)) {
                    var val = row.getAttribute('ows_'+arrColumns[i].name);
                    switch(arrColumns[i].type) {
                        case COC.SPList.STRING:
                            objRow[arrColumns[i].name] = val;
                            break;
                        case COC.SPList.NUMBER:
                            objRow[arrColumns[i].name] = COC.SPList.parseNumber(val);
                            break;
                        case COC.SPList.URL:
                            objRow[arrColumns[i].name] = COC.SPList.parseUrl(val);
                            break;
                        case COC.MULTILINESTRING:
                            objRow[arrColumns[i].name] = COC.SPList.parseMultilineString(val);
                            break;
                        default:
                            objRow[arrColumns[i].name] = val;
                            break;
                    }
                }
            }
            arrParsedRowData.push(objRow);
        }
        return arrParsedRowData;
    },

    // Create SOAP envelope instead of using SPServices.js so that it can work on calgary.ca or external sites via request webservice to jsonp response - e.g. See COC.Alertbox.js for SOAP request example that is used on calgary.ca and external sites
    //  soapConfig          object (optional)  parameters (all are optional):
    //    'listName'          string             name of list, default = 'Pages' - e.g. 'AlertBanner' or 'Pages'
    //    'query'             string             e.g. '<Where><And><Contains><FieldRef Name="FileLeafRef" /><Value Type="Text">.xls</Value></Contains><Geq><FieldRef Name="Modified" IncludeTimeValue="True" /><Value Type="DateTime">2010-05-10T11:53:32Z</Value></Geq></And></Where>'
    //    'fieldNames'        array<string>      array of internal field names - e.g. ['ows_Title', 'AlertMessage', 'Is_x0020_Public', 'Page_x0020_Path']
    //    'rowLimit'          number             rowLimit - default is 200, maximum is 5000
    //    'queryOptions'      string             e.g. '<ViewAttributes Scope='Recursive' />'   (NOTE: this is often used in combination with listName of 'Pages' to also get pages nested within child folders)
    //  @returns            string             string representation of SOAP <viewFields>...</viewFields>  - e.g. "<ViewFields><FieldRef Name='ows_Title' /><FieldRef Name='AlertMessage' /><FieldRef Name='Is_x0020_Public' /><FieldRef Name='Page_x0020_Path' /></ViewFields></viewFields>"
	createSoapEnvelope : function(soapConfig) {
        var soapConfig = (typeof soapConfig !== 'undefined') ? soapConfig : {'listName':'Pages'};
        soapConfig.listName = (typeof soapConfig.listName !== 'undefined') ? soapConfig.listName : 'Pages';

        function parseQuery() {
            if(!soapConfig.query || typeof soapConfig.query !== 'string')
                return '';
            return "<query><Query>" + soapConfig.query + "</Query></query>";
        }
        function parseViewFieldNames() {
            if(!soapConfig.fieldNames || !(soapConfig.fieldNames instanceof Array))
                return '';
            var i, strViewFields = "<viewFields><ViewFields>";
            for(i = 0; i < soapConfig.fieldNames.length; i++) {
                strViewFields += "<FieldRef Name='" + soapConfig.fieldNames[i] + "' />";
            }
            strViewFields +=  "</ViewFields></viewFields>";
            return strViewFields;
        }
        function parseRowLimit() {
            if(!soapConfig.rowLimit || typeof soapConfig.rowLimit !== 'number')
                return '';
            return "<rowLimit>" + soapConfig.rowLimit + "</rowLimit>";
        }
        function parseQueryOptions() {
            if(!soapConfig.queryOptions || typeof soapConfig.queryOptions !== 'string')
                return '';
            return "<queryOptions><QueryOptions>" + soapConfig.queryOptions + "</QueryOptions></queryOptions>";
        }
        var soapEnv =
            "<?xml version='1.0' encoding='utf-8'?>" +
            "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'>" + 
                "<soapenv:Body>" + 
                    "<GetListItems xmlns='http://schemas.microsoft.com/sharepoint/soap/'>" + 
                        "<listName>" + soapConfig.listName + "</listName>" + 
                        parseQuery() +
                        parseViewFieldNames() +
                        parseRowLimit() + 
                        parseQueryOptions() +  
                    "</GetListItems>" + 
                "</soapenv:Body>" + 
            "</soapenv:Envelope>";
        if(COC.Meta && COC.Meta.isTestMode)
            console.info('CoC Test - Soap envelope: ' + soapEnv);
		return soapEnv;
	},

    //SOAP code used in Greenline, written by Virgillio (nicely written, clean, easy to use).
    //TODO: get code snippet.

    //SOAP/jsonp on calgary.ca/snow > Weather widget
    //TODO: get code snippet.

    //SPServices snippets used recently in Real Estate project:
    //TODO: SPServices code to get list data from SharePoint list (brought in DOM via CQWP xslt): http://www.calgary.ca/CS/realestate/Pages/home.aspx
    //TODO: SPServices code (x2) to get list data from SharePoint list (brought in DOM via CQWP xslt): http://www.calgary.ca/CS/realestate/Pages/search.aspx

    //SPServices snippets used recently in Navigation project:
    //TODO: SPServices init to ensure SPServices.js script is loaded (like a "requires SPServices"): /SitePages/cocis/SubCategoryPage.js > InitSubCategoryPage
    //TODO: SPServices code to get list data from Taxonomy/Metadata field: /SitePages/cocis/SubCategoryPage.js > GetSubCategoryAndContentData
    //TODO: SPServices code to get list data from SharePoint list: /SitePages/cocis/SubCategoryPage.js > GetSubCategoryAndContentData


    /****************************** */
    /*   List result data parsers   */
    /****************************** */

    // item         XMLDocument object node                       XMLDocument element that contains z:row > attributes which can be found with $(item).attr('ows_Title');
    // column       string                                        SharePoint internal column/field name without the "ows_" prefix
    parseStringFromItem: function(item, column) {
        var spData = item.getAttribute('ows_'+column);
        if(!spData) return spData;
        return spData.trim();
    },

    // item         XMLDocument object node                       XMLDocument element that contains z:row > attributes which can be found with $(item).attr('ows_Title');
    // column       string                                        SharePoint internal column/field name without the "ows_" prefix
    parseNumberFromItem: function(item, column) {
        var spData = item.getAttribute('ows_'+column);
        if(!spData) return spData;
        return COC.SPList.parseNumber( spData );
    },
    // Parse data from Number type of SharePoint column/field. Follows string format: "[number]" or "[number].0" or "[number].00000000000000", etcetera (depending on decimal place setting of column). E.g. "2" or "2.00000000000000"
    // spData        string      SharePoint List Column/field data string (un-parsed SPList data is always a string)
    parseNumber: function(spData) {
        if(!spData) return spData;
        return parseInt( spData );
    },

    // item         XMLDocument object node                       XMLDocument element that contains z:row > attributes which can be found with $(item).attr('ows_Title');
    // column       string                                        SharePoint internal column/field name without the "ows_" prefix
    parseUrlFromItem: function(item, column) {
        var spData = item.getAttribute('ows_'+column);
        if(!spData) return spData;
        return COC.SPList.parseUrl( spData );
    },
    // Parse data from Url/Hyperlink/Picture type of SharePoint column/field. Follows string format: "[url], [description]". E.g. "http://www.calgary.ca/election/PublishingImages/profile-pictures/JohnDoe.jpg, https://www.calgary.ca/election/PublishingImages/profile-pictures/JohnDoe.jpg"
    // spData       string      SharePoint List Column/field data string (un-parsed SPList data is always a string)
    // @returns     object      object containing url and description of Url column in this format: { url: "...", description: "..." }
    parseUrl: function(spData) {
        if(!spData) return spData;
        var arrTmp = spData.split(', ');  //Assume it is here - sharepoint forces description string for every Url entered in (and defaults to using the Url as description if left blank)
        return { url: arrTmp[0].trim(), description: arrTmp[1].trim() };
    },

    // item         XMLDocument object node                       XMLDocument element that contains z:row > attributes which can be found with $(item).attr('ows_Title');
    // column       string                                        SharePoint internal column/field name without the "ows_" prefix
    parseMultilineStringFromItem: function(item, column) {
        var spData = item.getAttribute('ows_'+column);
        if(!spData) return spData;
        return COC.SPList.parseMultilineString( spData );
    },
    // Parse data from Text type of SharePoint column/field that has 'Multiline' setting turned on (which adds \r\n as line breaks). Follows string format: "[url], [description]". E.g. "http://www.calgary.ca/election/PublishingImages/profile-pictures/JohnDoe.jpg, https://www.calgary.ca/election/PublishingImages/profile-pictures/JohnDoe.jpg"
    // spData       string      SharePoint List Column/field data string (un-parsed SPList data is always a string)
    // @returns     array       array containing each "paragraph" of text from a multiline Text field where '\n' is the delimiter (and any extra line-breaks get discarded) - E.g. ["Lorem ipsum", "Si dolor"]
    parseMultilineString: function(spData) {
        if(!spData) return spData;
        var arrTmp = spData.split('\n');
        var i, paragraphs = [];
        for(i=0; i<arrTmp.length; i++) {
            var para = arrTmp[i].trim();
            if(para === '') continue;
            paragraphs.push(para);
        }
        return paragraphs;
    },


    /* Randall's Parser Snippets:
    
    //Sharepoint list parser:
    // parse the values after the # for returned items
    parsehash: function(item, values) {
        //split and remove null strings
        if (item.attr(values)) {
            var t = item.attr(values);
            t = t.split(';#');
            t = $.grep(t, function(n) {
            return (n);
            });
            return t;
        }
    },

    //Date parsers I used for getting dates from local storage.
    //they were line by line in large datasets:

    JsonDate: (dataset, field) => {
        var parseDateDMYZ = d3.time.format.iso.parse;
        dataset.forEach(function(d) {
            if (typeof d[field] == 'string') {
                d[field] = parseDateDMYZ(d[field]);
            }
        });
        return dataset;
    },

    JsonDateGrouped: (dataset, field, field2) => {
        var parseDateDMYZ = d3.time.format.iso.parse;
        dataset.forEach(function(d) {
            if (Array.isArray(d[field])) {
                d[field] = JsonDate(d[field], [field2]);
            }
        });
        return dataset;
    },
    */

};
//REUSABLE CODE BEGINS
//Much of this code could be rewritten and reused on a SPA or PWA as follows:
//1. Rewrite in Typescript or at least ES2016. Follow SOLID principles and instantiate classes with project-specific data or settings (e.g. pass in HtmlTemplates instead of hardcoding them in a class).
//2. Rewrite using a javascript framework.
//3. Use jekyll or similar solution to produce txt files containing sharepoint page content. Currently it is a manual process of copying things over.


var App = App || {};
// Track an event to WebTrends - // E.g: App.WT('Event name here','Additional detail');
// NOTE: Slight change to RegEx for "strComment" to also allow chars: ':/.-' so that we can write out URLs in comments for external link clicks..
// params	object		{ 'name':string, 'comment':string }   --> 'comment' is optional
App.WT = function(params) {   //Elections > WebTrendsTrack
	var dcCreator = "Election App";  // matches meta: DC.creator
	var wtTitle = "Vote 2017";  // matches meta: WT.ti
	var eName = params.name.trim().replace(/ /g, '').replace(/[^a-zA-Z_]/g, '');
	var eComment = (typeof params.comment !== 'undefined') ? params.comment.trim().replace(/ /g, '').replace(/[^a-zA-Z__:\/.-]/g, '') : '';
	//NOTE: latest WT documentation now instructs to use "[Event]:[Comment]" as the format with words condensed together (camel casing)
	var strEvent = (eComment !== '') ? eName + ":" + eComment : eName;

	dcsAction(dcCreator, wtTitle, strEvent);  // E.g. "Vote%202017/Nav:#viewCandidates"  (ie: on page title: "Vote%202017" user clicked a "view" navigation button that used view selector "#viewCandidates")
	//TEST: Log console instead of tracking event to test what would get logged
	//console.log("dcsAction(\""+dcCreator+"\", \""+wtTitle+"\", \""+strEvent+"\");  //log: " + encodeURIComponent(wtTitle) + "/" + strEvent);  // E.g. "Vote%202017/[EventString:CommentString]"  (ie: on page titled: "Vote 2017")
};
var COC = COC || {};
COC.qs = function (key) 
{
    key = key.replace(/[*+?^$.\[\]{}()|\\\/]/g, "\\$&"); // escape RegEx meta chars
    var match = location.search.match(new RegExp("[?&]"+key+"=([^&]+)(&|$)"));
    return match && decodeURIComponent(match[1].replace(/\+/g, " "));
};
COC.Analytics = COC.Analytics || {};
COC.Analytics.wt = COC.Analytics.wt || App.WT;  //replace COC.Analytics.wt with App.WT to support script from website in COC.Election.js


App.Init = new(function() {   //Elections > Init

    function init() {
		forceSSL();
		// fadeInBody();
    }
	// ensure page is being viewed over SSL so that geolocation will work
	function forceSSL() {
		if (location.protocol != 'https:')
			location.href = 'https:' + window.location.href.substring(window.location.protocol.length);	
	}
	// fade in body element for app-like startup effect
	// depends on inline-style: <body style="opacity:0">
	function fadeInBody() {
		var body = document.querySelector('body');
		App.Utils.fadeIn(body);
	}
	
	init();
})();


// Working with "add to home" plugin
App.AddToHome = new(function() {

	//Working with the Add to Home Screen plugin by cubiq: http://cubiq.org/add-to-home-screen, https://github.com/cubiq/add-to-homescreen
	var _ATH;	
	
	function init() {
		initEvents();
		initATHPlugin();
		handleATHOnPageLoad();
	}
	function initATHPlugin() {
		//trigger to load before ending body tag
		_ATH = addToHomescreen({ 
			appID: 'ca.calgary.vote2017',  //supports multiple uses of plugin on same domain
			autostart: false,  //needed to be able to manually trigger ATH popup to show
			skipFirstVisit: false,
			maxDisplayCount: 0,  // 0 = no limit of instances shown
			displayPace: 0,  // 0 = no limit of minutes
			lifespan: 99999999, //default is 15. Number of seconds before teardown. Instead of letting it teardown we have our own close button on the ATH popup. We manually remove it and the ATH plugin takes over to teardown associated events.
			icon: true,
			//message: '',  //Do not supply custom message. The risk of supplying custom message is that it overrides default plugin logic that chooses an internationalized message that is different between ios and android
			logging: false,  //helps with debugging failure to load ATH popup by seeing ATH plugin messages logged to console
			onShow: onShowATHPopup,
			onRemove: onRemoveATHPopup
		});
	}
	function initEvents() {
		//add click event to a button in global nav that triggers showing the "add to home" plugin popup
		
		var athTriggerPopupBtn = document.querySelectorAll('.homeScreenBtn')[0];
		athTriggerPopupBtn.addEventListener('click', function(e) {
		// jquery equivalent: $('.homeScreenBtn').click(function(e) {
			var athViewport = document.querySelectorAll('.ath-viewport')[0];
			if(athViewport)
				athViewport.parentElement.removeChild(athViewport);
			// jquery equivalent: $('.ath-viewport').remove();  //failproof to remove any existing ATH popup
			_ATH.shown = false; //failproof to ensure that it doesn't prevent from showing because ATH popup decides it has "already been shown on screen"
			_ATH.show({});
			COC.Nav.CloseNav();			
			e.preventDefault();
		});
	}
	function onShowATHPopup() {
		//we override ATH javascript animation with our own css animation
		var athContainer = document.querySelectorAll('.ath-container')[0];
		App.Utils.addClass(athContainer, 'coc-ath-show');
		App.Utils.removeClass(athContainer, 'coc-ath-hide');
		//jquery equivalent: $('.ath-container:first').addClass('coc-ath-show').removeClass('coc-ath-hide');
		var athViewport = document.querySelectorAll('.ath-viewport')[0];
		App.Utils.addClass(athViewport, 'coc-ath-show');
		App.Utils.removeClass(athViewport, 'coc-ath-hide');
		//jquery equivalent: $('.ath-viewport:first').addClass('coc-ath-show').removeClass('coc-ath-hide');
		
		//basically, we add html for our [close] button inside a "message" setting when initializing the add to home plugin
		//now that the popup is being shown we have to move our custom button out, then add on click. For accessibility we also add keydown functionality and set focus to it
		
		athContainer.innerHTML += '<span id="close-popup"><a role="button" class="cui btn-sm icon-only primary" tabindex="0">Close <span class="cicon cicon-times" aria-hidden="true"></span></a></span>';
		//jquery equivalent: $('.ath-container:first').append('<span id="close-popup"><a role="button" class="cui btn-sm icon-only primary" tabindex="0">Close <span class="cicon cicon-times" aria-hidden="true"></span></a></span>');
		
		var athPopupCloseBtn = document.querySelector('#close-popup').querySelector('a');
		// jquery equivalent: var athPopupCloseBtn = $('#close-popup a:first')
		athPopupCloseBtn.addEventListener('click', function(e) {
			closeATHPopup();
			e.preventDefault();
		});
		athPopupCloseBtn.addEventListener('keydown', function(e) {
			if (e.keyCode === 13)
				athPopupCloseBtn.click();
		});
		athPopupCloseBtn.focus();
	}
	function onRemoveATHPopup() {
		//Note: We wanted to ensure the ATH instance is destroyed after it is closed so that view transition animations perform well.
		//Was not able to explain why ATH plugin's "remove" functionality is triggered when you 1. Click to close ATH popup and then, 2. Access any nav menu in the web app. However, teardown at this point is good enough for our need to ensure performance.
		//This logged message proves that the instance is being destroyed properly with ATH plugin code at a key point before any view transitions would be triggered:
		console.log('Previous ATH instance destroyed');
	}
	function closeATHPopup() {
		//we override ATH javascript animation with our own css animation

		
		var athContainer = document.querySelectorAll('.ath-container')[0];
		App.Utils.addClass(athContainer, 'coc-ath-hide');
		App.Utils.removeClass(athContainer, 'coc-ath-show');
		//jquery equivalent: $('.ath-container:first').addClass('coc-ath-hide').removeClass('coc-ath-show');
		var athViewport = document.querySelectorAll('.ath-viewport')[0];
		App.Utils.addClass(athViewport, 'coc-ath-hide');
		App.Utils.removeClass(athViewport, 'coc-ath-show');
		//jquery equivalent: $('.ath-viewport:first').addClass('coc-ath-hide').removeClass('coc-ath-show');

		//Remove the ATH element on close after the custom "hide" animation should have completed - NOTE: The ATH plugin takes over later to teardown associated events that were created by plugin code:
		
		athViewport.addEventListener('fadeout', function() {
		// jquery equivalent: $('.ath-viewport:first').fadeOut(function() {
			athViewport.parentElement.removeChild(athViewport);
			// jquery equivalent: $(this).remove();
		});
	}
	//We check if an "ath" querystring parameter is present in URL to determine if this is a first visit (ie: no param) or if user accessed web app from icon on homescreen (ie: param exists).
	//Links to the web app do not have the parameter, indicating that it is user's first visit. Client-side code then adds "ath" parameter (ie: whenever using HTML5 pushstate we include the parameter) to indicate it is no longer user's first visit.
	//When user adds the app to home screen they essentially are bookmarking a URL to the web app that includes the "ath" parameter, signalling to our code when they load it up that it is 1. Not their first visit and 2. May have been launched from home screen.
	function handleATHOnPageLoad() {
		//if querystring "ath" is present in URL then show popup
		//if(COC.qs('ath') === null)
		//	showPopupOnLoad();
		if(COC.qs('pwa') !== null) {
			var body = document.querySelector('body');
			App.Utils.addClass(body, 'pwa-opened-from-homescreen');
		} else {
			// show "add to home" popup because detected not having opened from homescreen
			showPopupOnLoad();
		}
	}
	//NOTE: ATH plugin has an "autostart" setting but this prevents being able to manually trigger the ATH popup to show on button click, which we also want. That is why we manually show it with this function.
	function showPopupOnLoad() {
		//on initial DOM load the ATH plugin usually fails if we don't wait a moment to show it:
		setTimeout(function() {
			_ATH.show({});
		}, 100);
	}
	
	init();
})();


App.ViewSectionsFromTxt = new(function() {   //Used to load a view section that retrieves html from local txt via ajax

    function init() {
        initEvents();
    }
    function initEvents() {
		var content = document.querySelector('#content');
		initViewBtnClickEvents(content);
		var mobileBottomNav = document.querySelectorAll('.mobileBottomNav')[0];
    	initViewBtnClickEvents(mobileBottomNav);
    }
    function initViewBtnClickEvents(el) {
		if(el.length > 0) el = el[0];  // makes this compatable with jquery-selected input that may originate from .txt files
		[].slice.call(el.querySelectorAll('.view-button[data-txt-src]')).forEach(function(el, i) {
		//jquery equivalent: el.find('.view-button[data-txt-src]').click(function(e) {
			el.addEventListener('click', function(e) {
				var navItem = this;
				//jquery equivalent: var $navItem = $(this);
				//loadPageInIframe($navItem);
				loadHtmlTxtInSection(navItem);
				//trackEvent($navItem);  //now being handled from regular button click code
				//e.preventDefault();
			});
		});
	}
	// The content of most PWAs will not be cached so that it will request the latest version from the web. Only cache the PWA '/content/' folder if all content may be static.
    function loadHtmlTxtInSection(el) {
		var strHtmlTxtSrc = el.getAttribute('data-txt-src');
		//jquery equivalent: var strHtmlTxtSrc = $el.attr('data-txt-src');

        var profileId = getProfileIdFromQSParam(strHtmlTxtSrc);
        var forceReload = getReloadFromQSParam(strHtmlTxtSrc);

		var selSection = el.getAttribute('data-view-sel');
		//jquery equivalent: var selSection = $el.attr('data-view-sel');
		if(strHtmlTxtSrc && selSection) {
			var section = document.querySelector(selSection);
			var htmlLoadedFromTxt = section.querySelector('.html-txt');
			if(!profileId && !forceReload && htmlLoadedFromTxt)
				return;
			//jquery equivalent: if(!profileId && $(selSection).find('.html-txt').length !== 0) return;
			if(profileId) {
				if(htmlLoadedFromTxt)
					htmlLoadedFromTxt.parentElement.removeChild(htmlLoadedFromTxt);
				//jquery equivalent: $(selSection).find('.html-txt').remove();
		        COC.Election.Profile.Init(profileId);
			}
			
			// load html in to section from .txt file
			var request = new XMLHttpRequest();
			request.open('GET', strHtmlTxtSrc, true);			
			request.onload = function() {
			  if (request.status >= 200 && request.status < 400) {
				// Success!
				var data = request.responseText;

				// work with data
				var htmlTxt = '<div class="html-txt">'+ data +'</div>';
				// jquery equivalent: var $htmlTxt = $('<div class="html-txt">'+ data +'</div>');
				section.innerHTML = htmlTxt;
				// jquery equivalent: $(selSection).html($htmlTxt);
				
				// if any script elements existed in the .txt then execute these script within the DOM
				var scripts = section.querySelectorAll('script');
				for (var i = 0; i < scripts.length; i++) {
					eval(scripts[i].innerHTML); // run script
				}

        		App.ViewNav.initHtmlFromTxtEvents(section);
        		initViewBtnClickEvents(section);
			  } else {
				// We reached our target server, but it returned an error
			  }
			};
			request.onerror = function() {
				// There was a connection error of some sort
				// TODO: Write TDD spec that ensures it could not have been an error due to missing .txt files
				var viewLinkElAttributes = ' data-txt-src="'+strHtmlTxtSrc+'" data-view-sel="'+selSection+'" tabindex="0"';
				if(el.getAttribute('data-event')) viewLinkElAttributes += ' data-event="'+el.getAttribute('data-event')+'"';
				if(el.getAttribute('data-comment')) viewLinkElAttributes += ' data-comment="'+el.getAttribute('data-comment')+'"';
				var htmlTxt = '<div style="padding-top:60px;"><div>Your internet connection does not seem to be working because, there was a problem retrieving content. Please check that you are able to browse the internet then retry.</div><div><a class="view-button cui btn-md secondary-ghost" '+viewLinkElAttributes+'>Retry</a></div></div>';
				section.innerHTML = htmlTxt;
				// jquery equivalent: $(selSection).html($htmlTxt);
				initViewBtnClickEvents(section);
				App.ViewNav.initHtmlFromTxtEvents(section);
			};
			request.send();

			/*
			// jquery equivalent:
			var jqxhr = $.ajax({
				'url': strHtmlTxtSrc,
				'dataType': 'html'
			})
			.done(function(data) {
				var htmlTxt = '<div class="html-txt">'+ data +'</div>';
				//jquery equivalent: var $htmlTxt = $('<div class="html-txt">'+ data +'</div>');
				section.innerHTML = htmlTxt;
				//jquery equivalent: $(selSection).html($htmlTxt);
				
				// if any script elements existed in the .txt then execute these script within the DOM
				var scripts = section.querySelectorAll('script');
				for (var i = 0; i < scripts.length; i++) {
					eval(scripts[i].innerHTML); // run script inside div
				}

        		App.ViewNav.initHtmlFromTxtEvents(section);
        		initViewBtnClickEvents(section);
			})
			.fail(function() {
				var $htmlTxt = $('<div>There was a problem retrieving content for this section</div>');
        		$(selSection).html($htmlTxt);
			});
			*/
	    }
    }
	function getProfileIdFromQSParam(txt) {
	    var match = txt.match(new RegExp("[?&]profileId=([^&]+)(&|$)"));
	    return match && decodeURIComponent(match[1].replace(/\+/g, " "));
	}
	function getReloadFromQSParam(txt) {
	    var match = txt.match(new RegExp("[?&]reload=([^&]+)(&|$)"));
	    return match && decodeURIComponent(match[1].replace(/\+/g, " "));
	}
    function trackEvent(el) {
 	    // track WebTrends event for element - consider refactoring in to a global function
		var strEvent = el.getAttribute('data-event');
		// jquery equivalent: var strEvent = $el.attr('data-event');
        var strComment = $el.getAttribute('data-comment');
        // jquery equivalent: var strComment = $el.attr('data-comment');
        if(strEvent && strComment)
	        App.WT({'name':strEvent, 'comment':strComment});
    }
    /* This method proved much slower than loading with ajax from txt as html:
    function loadPageInIframe($el) {
        var strIframeSrc = $el.attr('data-iframe-src');
		var selSection = $el.attr('data-view-sel');
		if(strIframeSrc && selSection) {
			if($(selSection).find('iframe').length !== 0)
				return;
				
	        var $iframe = $('<iframe/>', {
	            src: strIframeSrc,
	            style: 'height:100%;width:100%;border:0;'
	        });
	        $iframe.on('load', function() {
        		$(this).contents().find('#cocis-content a, #cocis-maincontent a').each(function() {
					var aHref = $(this).attr('href');
					if(aHref.search('calgary.ca') > -1) {
						alert('found a calgary.ca link');
					}
				});
			});
	        $(selSection).html($iframe);
		}				
    }*/

    init();
    
    return {
    	'loadHtmlTxtInSection': loadHtmlTxtInSection,
    	'initViewBtnClickEvents': initViewBtnClickEvents,
    };
})();


/* Not being used - was written to control header buttons that would switch between sibling views
App.View = new(function() {   //Elections > View
    function init() {
        initEvents();
    }
    function initEvents() {
        $('.coc-wrapper > main > section.view > header button').on('click', clickButton);
        // jquery equivalent: $('.coc-wrapper > main > section.view > header button').on('click', clickButton);
    }
    function clickButton() {
        //hideViewSections();
        $(this).siblings().removeClass('primary-ghost');
        $(this).addClass('primary-ghost');
        //$($(this).attr('data-view-sel')).show().siblings('section.view').hide();
        console.log($(this).attr('data-view-sel'));
    }

    init();
})();
*/


App.GlobalNav = new(function() {
    function init() {
	    COC.Nav.Init();
	    COC.Nav.InitLocalNav();
	    COC.Nav.InitMobileBottomNav();
	}
	init();
})();


// This handles the basic switching of views and child views. This tracks events to WebTrends and handles html5 "push state" for navigation back and forward through history of views.
// Links in the footer nav are handled here.
// Links in the "Information for Voters" local nav are handled here. Those links also have additional behaviour in a different class that loads page data from calgary.ca.
App.ViewNav = new(function() {   //Elections > ViewNav
    function init() {
        initEvents();
		trackAppInitEvent();
        switchViewOnInit();
    }
    function initEvents() {
		initLinkEventsOnEl(document);
        detectPopChangeEvent();
	}
    function initHtmlFromTxtEvents(section) {
		if(section.length > 0) section = section[0];  // makes this compatable with jquery-selected input that may originate from .txt files
		initLinkEventsOnEl(section);
    }
	function initLinkEventsOnEl(el) {
		[].slice.call(el.querySelectorAll('.view-button')).forEach(function(el, i) {
			el.addEventListener('click', clickButton);
		});
        // jquery equivalent: $el.find('.view-button').on('click', clickButton);
		[].slice.call(el.querySelectorAll('.external-link')).forEach(function(el, i) {
			el.addEventListener('click', clickExtLink);
		});
		// jquery equivalent: $el.find('.external-link').on('click', clickExtLink);
		[].slice.call(el.querySelectorAll('a')).forEach(function(el, i) {
			// check if hyperlink is an anchor link
			var aHref = el.getAttribute('href');
			// apply only to <a> elements with a valid href attribute and prevent in cases where href is a) missing, b) empty, c) contains only '#', or d) contains 'javscript:'
			var isAnchorLink = (aHref && aHref !== null && aHref.charAt(0) === '#' && aHref.length > 1 && aHref.search('javascript:') < 0);
			if(isAnchorLink)
				el.addEventListener('click', clickAnchorLink);
		});
	}
	// pop change event occurs when page loads so we can handle view changes with back/forward browser buttons
    function detectPopChangeEvent() {
		window.onpopstate = function(e) {
		  if (e.state) { // history changed because of back/forward and the history entry was made with Html5 pushState or replaceState
	        var selView = e.state.selView;  //e.g: #viewCandidates
			var viewBtnEl = document.querySelector('.view-button[data-view-sel="'+ selView +'"]');
			// jquery equivalent: var $viewBtnEl = $('.view-button[data-view-sel="'+ selView +'"]').first();
		  	goToView(viewBtnEl, false);
		  } else { // history changed because of back/forward and returning to the initial "page load" state
		    switchViewOnInit();  //NOTE: This was breaking anchor links meant to go to a section of the current view and it is not needed
		  }
		}
    }
	// tracks an event to track when app initialized
	function trackAppInitEvent() {
		App.WT({'name':'Init'});
	}
	// goes to initial view (currently it sets initial view to "Vote" view button). Used on app load and also if user returns to initial screen by traversing back through pushState in browser history.
    function switchViewOnInit() {
    
    	//$('#btnVote').click();    	

    	//Go to btn vote element without clicking on it because, we don't want nav to collapse like it does on click.
		var btnVote = document.querySelector('#btnVote.view-button');
		// jquery equivalent: var $btnVote = $('#btnVote.view-button');
    
    	//This loads html txt in section if applicable
		//App.ViewSectionsFromTxt.loadHtmlTxtInSection($btnVote);
    	
    	goToView(btnVote, false);
    	clickButtonDelayPushState(btnVote);
    }
    function clickButtonDelayPushState(btnView) {
        goToView(btnView, false);
        
        setTimeout(function() {
	        //push view to browser history
			var selView = btnView.getAttribute('data-view-sel');
			// jquery equivalent: var selView = $btnView.attr('data-view-sel');
	        if(selView) {
				var strViewTitle = btnView.getAttribute('data-view-title');  //e.g: Candidates
				// jquery equivalent: var strViewTitle = $btnView.attr('data-view-title');  //e.g: Candidates
		        pushState(strViewTitle, selView);
		    }
		}, 300);
    }
    function clickButton() {
		var btnView = this;
		// jquery equivalent: var $btnView = $(this);
        goToView(btnView, true);
    }
    function goToView(btnView, doPushState) {
		//$btnView.siblings().removeClass('primary');
		//$btnView.addClass('primary');
		var selView = btnView.getAttribute('data-view-sel');
		// jquery equivalent: var selView = $btnView.attr('data-view-sel');
        if(selView) {
	    	App.Utils.removeClass(document.querySelector('.view-button.active'), 'active');
	    	// jquery equivalent: $('.view-button.active').removeClass('active');
	    	App.Utils.addClass(document.querySelector('.view-button[data-view-sel="'+selView+'"]'), 'active');
	    	// jquery equivalent: $('.view-button[data-view-sel="'+selView+'"]').addClass('active');
	
	        hideAllViews();
	        
	        //show view
			var view = document.querySelector(selView);
			// jquery equivalent: var $view = $(selView);
	    	App.Utils.removeClass(view, 'hide');
	    	App.Utils.addClass(view, 'show');
	        // jquery equivalent: $view.removeClass('hide').addClass('show'); //.show();
	        
			showMainChildView(view);
			showParentView(view);
			
			document.querySelector('#content').scrollTop = 0;  //scroll back to top of main content area
			// jquery equivalent: $('#content').scrollTop(0);  //scroll back to top of main content area
			        
	        if(doPushState) {
		        //push view to browser history
		        var strViewTitle = btnView.getAttribute('data-view-title');  //e.g: Candidates
		        // jquery equivalent: var strViewTitle = $btnView.attr('data-view-title');  //e.g: Candidates
		        pushState(strViewTitle, selView);
	        }
	    }
        //always track because of an edge case: We track an event on "Add to Home Screen" button which doesn't change the view but is given class "view-button"
        trackEvent(btnView); // track event on element
    }
    function clickExtLink() {
		var hyperlink = this;
		// jquery equivalent: var $hyperlink = $(this);
        trackEvent(hyperlink); // track event on element
	}
	function clickAnchorLink(e) {
		// manually click on anchor element then, if local navigation pattern is visible also adjust scroll location so that anchor target is visible beneath floating nav
		var aHref = e.target.getAttribute('href');
		document.querySelector(aHref).scrollIntoView(true);  // e.g. querySelector('#anchorTargetElId')
		// check if local nav is present (and there must also be a main #container element)
		if(document.querySelectorAll('#localNavContainer .nav-menu').length > 0 && document.querySelector('#content')) { 
			// now make up for the additional pixels to scroll past the local nav so that the anchor link target is fully in view and not obscured behind it
			//var localNavHeight = App.Utils.outerHeight(document.querySelector('.navBtnMobile_topic.expandBtn'));
			setTimeout(function() { document.querySelector('#content').scrollTop -= 49; }, 100); 
			// jquery equivalent: setTimeout(function() { $('#content').scrollTop( $('#content').scrollTop() - 49 ); }, 100); 
		}
		e.preventDefault();
	}
    function hideAllViews() {
		[].slice.call(document.querySelectorAll('section.view')).forEach(function(el, i) {
			App.Utils.removeClass(el, 'show');
			App.Utils.addClass(el, 'hide');
		});
		// jquery equivalent: $('section.view').addClass('hide').removeClass('show'); //.hide();
    }
    //this performs functionality on child views if the $view element is a parent view
    function showMainChildView(view) {
		if(App.Utils.hasClass(view.parentElement, 'view')) return;
    	// jquery equivalent: if($view.parent('section.view').length > 0) return;
    	//get the "main child" view of this parent view
		view = view.querySelector('section.view.main-child');
		if(view === null) return;
		// jquery equivalent: $view = $view.find('section.view.main-child');
        //if parent view and all child views hidden then show "main child view"
		App.Utils.removeClass(view, 'hide');
		App.Utils.addClass(view, 'show');
		// jquery equivalent: $view.addClass('show').removeClass('hide'); //.show();
        if(view.getAttribute('id') === 'viewSectionVoterInfo_WhyVote') {
		// jquery equivalent: if($view.attr('id') === 'viewSectionVoterInfo_WhyVote') {
			var navItem = document.querySelector('#localNavContainer .view-button[data-view-sel="#viewSectionVoterInfo_WhyVote"]');
			// jquery equivalent: var $navItem = $('#localNavContainer .view-button[data-view-sel="#viewSectionVoterInfo_WhyVote"]');
			navItem.click();
			// jquery equivalent: $navItem.click();
		}
    }
    //this performs functionality on parent view if the $view element is a child view
    function showParentView(view) {
    	var parentView = view.parentElement;
		if(!parentView) return;
    	// jquery equivalent: if($view.parent('section.view').length > 0) return;
		App.Utils.removeClass(parentView, 'hide');
		App.Utils.addClass(parentView, 'show');
		// jquery equivalent: $view.parent('section.view').addClass('show').removeClass('hide'); //.show();
    }

    function pushState(strViewTitle, selView) {
	    var stateObj = { 'selView': selView };  //{ foo: "bar" };
	    var selView = "Default.aspx" + selView;  //e.g: Default.aspx#viewCandidates
		//history.pushState({}, strViewTitle, selView);
		//NOTE: adding ? to URL so that "add to homescreen" plugin can check if web app was loaded from homescreen (see "detectHomescreen" option: http://cubiq.org/add-to-home-screen):
		//history.pushState(stateObj, strViewTitle, 'Default.aspx');  //trying this out instead where state always shows main page but, the stateObj contains the selView value used when user goes back or forward in browser history
		history.pushState(stateObj, strViewTitle, selView);  //trying this out instead where state always shows main page but, the stateObj contains the selView value used when user goes back or forward in browser history
    }
    // data-event attribute mandatory on $el
    // data-comment attribute is needed if it is a view-button. Else, href attibute is needed if it is an external-link
    function trackEvent(el) {
 	    // track WebTrends event for element - consider refactoring in to a global function
		var strEvent = el.getAttribute('data-event');
		// jquery equivalent: var strEvent = $el.attr('data-event');
        var strComment = el.getAttribute('data-comment') || el.getAttribute('href');
        // jquery equivalent: var strComment = $el.attr('data-comment') || $el.attr('href');
        if(strEvent && strComment) {
			if(App.Utils.hasClass(el, 'external-link')) {
        	// jquery equivalent: if($el.hasClass('external-link')) {
				strComment = 'External_'+strComment;
			}
	        App.WT({'name':strEvent, 'comment':strComment});
	    } else {
	    	console.error('CoC: html element missing required attribute needed for WebTrends');
	    }
    }
    init();
    
    return {
    	'initHtmlFromTxtEvents': initHtmlFromTxtEvents
    }
})();

var Elect = App;  // makes this file backwards-compatible with old namespace outside scripts may depend on
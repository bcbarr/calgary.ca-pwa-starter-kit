var App = App || {};
App.Utils = new(function() {
	function addClass(el, className) {
		if(el == null) return;
		if(el.classList)
			el.classList.add(className);
		else
			el.className += ' ' + className;
	}
	function removeClass(el, className) {
		if(el == null) return;
		if(el.classList)
			el.classList.remove(className);
		else
			el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
	}
	function hasClass(el, className) {
		if(el == null) return null;
		if(el.classList)
			el.classList.contains(className);
		else
			new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className);
    }
    function outerHeight(el) {
        var height = el.offsetHeight;
        var style = getComputedStyle(el);      
        height += parseInt(style.marginTop) + parseInt(style.marginBottom);
        return height;
	}
	function fadeIn(el) {
		el.style.opacity = 0;	  
		var last = +new Date();
		var tick = function() {
			el.style.opacity = +el.style.opacity + (new Date() - last) / 400;
			last = +new Date();			
			if (+el.style.opacity < 1) {
				(window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
			} else {
				(window.requestAnimationFrame && requestAnimationFrame(show)) || setTimeout(show, 16);
			}
		};
		var show = function() {
			el.style.opacity = '';
		};	  
		tick();
	}

    return {
    	'addClass': addClass,
    	'removeClass': removeClass,
        'hasClass': hasClass,
		'outerHeight':outerHeight,
		'fadeIn':fadeIn,
    };
})();
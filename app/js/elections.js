﻿//COC.Election.js overrides for elections mobile web app
//NOTE: These HtmlTemplates are overrides to pre-existing HtmlTeplates from Elections website, in COC.Election.js. We override these so they'll work with the web app.
var COC = COC || {};
COC.Election = COC.Election || {};
COC.Election.HtmlTemplates = {
	WardTableFindMyWard: function(strWard) {
		return '. You will be in either <span style="font-weight:bold;">Ward ' + strWard + '</span>. Please find your exact ward by address.<br /><a class="cui btn-lg secondary view-button" data-view-sel="#viewSectionVoterInfo_FindMyWard" data-view-title="Find my Ward" data-event="Content" data-comment="Find my Ward:From Community Table:Multi-Ward" data-iframe-src="election/Pages/information-for-voters/find-my-ward.aspx?IsDlg=1&iframe=1" data-txt-src="content/voterInfo/find-my-ward.html.txt">Find my Ward</a>';
	},
	WardTableReviewCandidates: function(strWard) {
		return ' will be in <span style="font-weight:bold;">Ward ' + strWard + '</span>. Please review your candidates.<br /><a class="cui btn-lg secondary view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward'+strWard+'" data-view-title="Ward '+strWard+'" data-event="Content" data-comment="Ward '+strWard+':From Community Table" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-'+strWard+'.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward'+strWard+'.txt">Councillor Candidates</a> <a class="cui btn-lg secondary view-button" data-view-sel="#viewSectionCandidates_School_Ward'+strWard+'" data-view-title="Ward '+strWard+'" data-event="Content" data-comment="Ward '+strWard+':From Community Table" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/ward-'+strWard+'.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board-ward'+strWard+'.txt">School Board Candidates</a>';
	},
	FindMyWardLookupResult: function(strWard) {
		return ' Please review your candidates.<p><a class="cui btn-lg secondary view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward'+strWard+'" data-view-title="Ward '+strWard+'" data-event="Content" data-comment="Ward '+strWard+':From Community Table" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-'+strWard+'.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward'+strWard+'.txt">Councillor Candidates</a> <a class="cui btn-lg secondary view-button" data-view-sel="#viewSectionCandidates_School_Ward'+strWard+'" data-view-title="Ward '+strWard+'" data-event="Content" data-comment="Ward '+strWard+':From Community Table" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/ward-'+strWard+'.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board-ward'+strWard+'.txt">School Board Candidates</a></p>';						
	},
	ProfileBtnBackToMayoral: function() {
		return '<a class="cui btn-lg secondary view-button" data-view-sel="#viewSectionMayoral_Mayoral" data-view-title="ProfileMayoral" data-event="Candidates" data-comment="Mayoral" data-iframe-src="election/Pages/meet-the-candidates/mayoral/default.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/mayoral.txt">&lt; Candidates</a>';
	},
	ProfileBtnBackToCouncillorWard: function(strWard) {
		return '<a class="cui btn-lg secondary view-button" data-view-sel="#viewSectionCandidates_Councillors_Ward'+strWard+'" data-view-title="Ward '+strWard+'" data-event="ProfileCouncillor" data-comment="Ward '+strWard+'" data-iframe-src="election/Pages/meet-the-candidates/councillor/ward-'+strWard+'.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/councillor-ward'+strWard+'.txt">&lt; Candidates for Ward '+strWard+'</a>';
	},
	ProfileBtnBackToSchoolWard: function(strWard) {
		return '<a class="cui btn-lg secondary view-button" data-view-sel="#viewSectionCandidates_School_Ward'+strWard+'" data-view-title="Ward '+strWard+'" data-event="ProfileSchool" data-comment="Ward '+strWard+'" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/ward-'+strWard+'.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board-ward'+strWard+'.txt">&lt; Candidates for Ward '+strWard+'</a>';
	},
	ProfileBtnBackToSeparateSchool: function() {
		return '<a class="cui btn-lg secondary view-button" data-view-sel="#viewSectionCandidates_School_OtherCandidates" data-view-title="Other Candidates" data-event="ProfileOtherSchool" data-comment="Other Candidates" data-iframe-src="election/Pages/meet-the-candidates/school-trustee/other-separate-school-candidates.aspx?IsDlg=1&iframe=1" data-txt-src="content/candidates/school-board-other-separate-school-candidates.txt">&lt; Candidates</a>';
	},
};
